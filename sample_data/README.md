# Sample Data for Testing

This folder contains files to test the following endpoints

## /labels

* test_labels.ttl : Sample URIs for each of the supported authorities. Each endpoint will query the chosen authority for those URIs to fetch rdfs:label values for each entity. Examples of predicates to skip such as 'owl:sameAs' are also included.

## /minting

* test_minting.ttl : Sample placeholders for testing.
* minting_patterns.ttl : file with sample patterns to capture placeholders.

## /redirect

* test_redirects.ttl : Sample Wikidata and VIAF URIs which have redirects. Test by sending
                       a request to either Wikidata or VIAF to see the old URIs replaced by redirected URIs.

## /replacement
    
* test_replacement.ttl : Use as data_file
* redirects.json          : Sample wikidata URI mapping, to be used as map_file. Test by sending a request to the replacement endpoint to see the URI replaced by the mapping.
* entity_replacements.tsv (regex test) : Sample replacement patterns, to be used as map_file. Set parameter 'regex' to True and Test by sending a request to the replacement endpoint to see the prefix for viaf change from 'https://viaf.org/viaf/' to 'http://viaf.org/viaf/'


## /validate

* valid.ttl   : Valid sample rdf data
* invalid.ttl : Invalid sample rdf data, missing a dot to terminate sentence
* test_prefix_validation.ttl : Contains an example of an unrecognized prefix and a prefix with an error for the endpoint to suggest a close match
* test_sparql_validation.ttl : Contains example of incomplete data and potential errors found with several SPARQL queries
* test_uri_validation.ttl : Contains examples of incorrect entity URIs.
* test_vocab_validation.ttl : Contains exammples of valid and invalid LINCS vocabulary terms.

## /combine
* two or more unique valid rdf files can be used as input. Example are test_combine_01.ttl and test_combine_02.ttl


## /coordinates
* test_coordinates.ttl


## /ttl_split
* any valid rdf file in turtle format can be used as input. test_split.ttl is one option.


## /conversion/x3ml/custom
* files in x3ml_files/ can be used as input. input.xml is the input data, mapping.x3ml is the x3ml mapping file, policy.xml is the generator policy.


## /serialize
* any valid rdf file can be used as sample input
