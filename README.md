# [Linked Data Enhancement API](https://lincsproject.ca/docs/tools/linked-data-enhancement-api/)

This is an API that bundles common cleaning and validation steps for data that has already been converted into RDF. We use this as a final step before moving the data into the LINCS knowledge graph. Most commonly within LINCS, this is used for data after processing in X3ML, but some of the steps apply to data that came out of the TEI or NLP conversion workflows. We recommend you use the API with RDF in turtle format (.ttl) as we have not tested all other serializations of RDF.

To learn more about how to apply these steps to your data creation process, see our [Publish Data with LINCS documentation](https://lincsproject.ca/docs/create-data/publish-data/).

## Running the API locally using Docker

From the main directory that contains `Dockerfile`, run the following two commands one after another:

```bash

docker build --no-cache -t postprocess .

docker run -p 8080:80 -t -i postprocess
```

Alternatively, you can also run with Docker Compose:

```bash
docker compose up --build
```

The API is now accessible locally at <http://127.0.0.1:8080> or <http://0.0.0.0:8080> depending on your machine.

## Examples and Documentation

### Wiki

A comprehensive explanation of all endpoints in the API, including information on running and testing them, can be found in this repo's [wiki.](https://gitlab.com/calincs/conversion/post-processing-service/-/wikis/home)

### Jupyter Notebook

A tutorial of the API in a Jupyter notebook is available [here](https://gitlab.com/calincs/conversion/post-processing-service/-/blob/main/example_usage/UPDATES_NEEDED_LD_enhancement_api.ipynb).

### Postman

Example usage as a Postman collection is available [here](https://gitlab.com/calincs/conversion/post-processing-service/-/blob/main/example_usage/example_requests_postman_collection.json).
