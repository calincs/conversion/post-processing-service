# Start from python 11 image and Debian 12 which is compatible with jdk 17
FROM python:3.11-slim-bookworm

# dependency for installing jdk
RUN mkdir -p /usr/share/man/man1

# Install OpenJDK-17
RUN apt-get update && \
    apt-get install -y openjdk-17-jdk-headless && \
    apt-get install -y ant && \
    apt-get clean;

# Fix certificate issues
RUN apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f;

# Setup JAVA_HOME -- useful for docker commandline
ENV JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
RUN export JAVA_HOME

# Setup JENA_HOME and PATH
ENV JENA_HOME=/code/app/data/apache-jena-5.1.0
RUN export PATH=$PATH:$JENA_HOME/bin

WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./app /code/app
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]