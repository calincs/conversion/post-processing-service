import os
import requests
import zipfile


# TODO: test this script and ensure up to date

input_file = 'sample_data/test_large_viaf_files.ttl'
output_file_w_duplicates = 'sample_data/viaf_labels_w_duplicates.ttl'
output_file = 'sample_data/viaf_labels.ttl'
authority = 'viaf'

# initialize output file
out_file  = open(output_file_w_duplicates,'wb')
out_file.close()

file = {
    'file': (input_file, open(input_file, 'rb')),
}

params = (
    ('file_size', '2000'),
)

response = requests.post('http://0.0.0.0:80/split/', params=params, files=file)

# write zip file locally
zipfilepath = 'sample_data/split_files.zip'
with open(zipfilepath, 'wb') as file:
    file.write(response.content)

# iterate through zip file

#Iterate through files in zip file
first = True
with zipfile.ZipFile(zipfilepath, "r") as f:
    for name in f.namelist():
        #Read contents of the file
        filecontents = f.read(name)

        #send each file to viaf
        params = (
        )
        files = {
            'file': filecontents,
        }

        response = requests.post('http://0.0.0.0:80/labels/{}'.format(authority), params=params, files=files)

        # write label endpoint zip file locally
        labelzipfilepath = 'sample_data/response_files.zip'
        with open(labelzipfilepath, 'wb') as file:
            file.write(response.content)

        zip = zipfile.ZipFile(labelzipfilepath)
        
        #Iterate through files in zip file
        for zipfilename in zip.filelist:
            
            #Read contents of the file
            content = zip.read(zipfilename)



            with open(output_file_w_duplicates,'ab') as out_file:
                for line in content.replace(b'\r\n', b'\n').split(b'\n'):
                    if first:
                        out_file.write(line)
                        out_file.write(b'\n')          
                    else:
                        if line.startswith(b'@prefix') or line.startswith(b'{'):
                            continue
                        out_file.write(line)
                        out_file.write(b'\n')
    
        first = False

# check for duplicates

lines_seen = set() # holds lines already seen
outfile = open(output_file, "w")
for line in open(output_file_w_duplicates, "r"):
    if line not in lines_seen: # not a duplicate
        outfile.write(line+'\n')
        lines_seen.add(line)
outfile.close()

#cleanup
os.remove('sample_data/response_files.zip')
os.remove('sample_data/split_files.zip')
os.remove(output_file_w_duplicates)