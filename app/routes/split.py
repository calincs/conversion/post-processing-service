from fastapi import APIRouter, File
from starlette.responses import Response
from app.utils.utils import create_data_dir
from typing import Optional

import io
import os
import shutil
import zipfile

router = APIRouter()

"""
TODO
- see if this can be done with rdflib instead

"""


# Adapted code from https://gitlab.com/calincs/conversion/leaf-pipeline/-/blob/main/app/utils/request_helpers.py
@router.post("", status_code=200)
@router.post("/", status_code=200)
def split_rdf(file: bytes = File(...), file_size: int = None, encoding: Optional[str] = "utf-8"):

    # initalize counters and write to a file
    count_triples, count_prefixes, triple_counter = 0, 0, 0

    # Read that file and count the number of dots and prefixes and append the prefixes
    prefix_list = []

    if not file:
        return "Please input data file"

    file_content = file.decode(encoding).split('\n')

    for i in range(0, len(file_content)):
        if file_content[i].startswith('@prefix'):
            prefix_list.append(file_content[i].strip("\n"))
            count_prefixes += 1
        elif file_content[i].rstrip().endswith('.'):
            count_triples += 1
    prefix_list = list(set(prefix_list))
    prefix_list.sort()

    # Count the number of triples there are and divide them up and then set the starting line to be after the prefix
    last_line = count_prefixes

    # Get the split value based on the number of files we want to have
    split_value = file_size
    result = count_triples // split_value

    # Loop to the amount of split files we want to write
    output_path = create_data_dir("", "/code/app/data/split_files/")

    for i in range(0, result + 1):

        # write to a file write the prefix list to the file
        with open(output_path + '/split_file_' + str(i) + '.ttl', 'w', encoding=encoding) as f_out:
            for prefix in prefix_list:
                f_out.write(prefix + "\n")
            f_out.write("\n")

            # Starting from the last line (Treating Last line variable as a pointer)
            for i in range(last_line, len(file_content)):

                # check if a string ends with a . if then add to counter until it hits a certain value
                if file_content[i].rstrip().endswith('.'):
                    f_out.write(file_content[i] + "\n")
                    triple_counter += 1

                    # Once 2000 triples are stored then set the next line to be a different value and then reset the count and break the for loop
                    if triple_counter >= split_value:
                        last_line = i + 1
                        triple_counter = 0
                        break

                    # if the string doesn't end with a . simply write to the file
                else:
                    f_out.write(file_content[i] + "\n")
        # return on success

    # Saving reference to response object
    zip_filename = "split_files.zip"
    s = io.BytesIO()
    zf = zipfile.ZipFile(s, "w")

    for subdir, dirs, files in os.walk(output_path):
        for file in files:
            zf.write(output_path + "/" + file, file)

    # Must close zip for all contents to be written
    zf.close()

    # Grab ZIP file from in-memory, make response with correct MIME-type
    resp = Response(s.getvalue(), media_type="application/x-zip-compressed", headers={
        'Content-Disposition': f'attachment;filename={zip_filename}'
    })

    # cleanup
    shutil.rmtree(output_path)
    return resp
