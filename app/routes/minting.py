from fastapi import APIRouter, File
from starlette.responses import Response
from typing import List, Optional
from app.utils.utils import create_data_dir

import json
import re
import os
import shutil
import requests

router = APIRouter()


"""
TODO:
- this needs to be tested within the conversion workflow and likely move it to a separate API. for now removed from postman examples and service.

"""

def get_uid(count):
    '''
    requests count # of LINCS URIs from the LINCS UID generator service
    when hosted on LINCS servers, this will default to the official LINCS UID generator
    when run locally, this will default to a locally run LINCS UID generator service
    or locally, set this as MINT_URL environment variable
    '''
    mint_url = os.environ.get("MINT_URL")
    if not mint_url:
        mint_url = "http://localhost:5000/generateMultipleUID"
    try:
        print(f"Requesting URIs from {mint_url}")
        response = requests.get(mint_url, params={'num': str(count)})
        uuid = response.json()["uids"]
    except KeyError:
        print(response.status_code, ":  Failed to get UID: ", response.content)
        uuid = None
    return uuid


@router.post("/", status_code=200)
@router.post("", status_code=200)
def get_entities(pattern_file: bytes = File(...), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    temp_list = []  # list of temp placeholders
    patterns = []  # list of patterns
    new_prefixes = []
    new_uris = []

    # handle missing data file
    if not file[0]:
        return "Please input data file"
    file_contents = file[0].decode(encoding)

    # extract patterns
    if pattern_file:
        content = pattern_file.decode(encoding)
        lines = content.split('\n')
        patterns = [line.strip('r"').strip('"\r') for line in lines]
    else:
        print('Pattern file not found, default pattern used')
        patterns.append(r"temp_lincs_temp:([A-Za-z0-9\#].*?)[ ;,\.]")  # uses default pattern if no pattern provided.

    # check for defined prefixes
    for pattern in patterns:
        if pattern.startswith('@'):
            line = pattern.split(' ')

            # add the prefix part to new_prefixes to use for finding values
            new_prefixes.append(line[1].strip(':'))

            # add the uri part to new_uris to use for fidning values
            new_uris.append(line[2].strip('<').strip('>'))

    # remove empty values from list
    patterns = list(filter(None, patterns))

    for pattern in patterns:

        # find matching values
        temp_lincs = re.findall(pattern, file_contents)

        # fetch prefix for IDs found
        for placeholder in temp_lincs:
            prefixes = re.findall(r"(\w*)(?=:{})".format(re.escape(placeholder)), file_contents)
            if prefixes:
                # add prefix with uri to temp_list
                temp_list.append(prefixes[0] + ':' + placeholder)

                # Use the prefix to see if it has a full uri defined for it at the top of the file
                full_uri = re.findall(r'@prefix {}: (.*?) .'.format(re.escape(prefixes[0])), file_contents)
                if full_uri:
                    new_uris.append(full_uri[0])

            # searches for URIs in full
            if not prefixes:
                prefixes = re.findall(r"<(.*)(?=/{})".format(re.escape(placeholder)), file_contents)
                if prefixes:
                    # Use the full uri to see if it has a prefix defined for it at the top of the file
                    full_uri = re.findall(r'@prefix (.*?): <{}/> .'.format(re.escape(prefixes[0])), file_contents)
                    if full_uri:
                        new_prefixes.append(full_uri[0])

                # if no prefixes found, continue to the next pattern
                if not prefixes:
                    continue
                temp_list.append(prefixes[0] + '/' + placeholder)

    # find results using new prefixes
    new_uris = set(new_uris)
    for pattern in new_uris:
        pattern = pattern.strip('>')
        results = re.findall(r"{}([0-9|A-Za-z]+)".format(re.escape(pattern)), file_contents)
        for result in results:
            temp_list.append(pattern.strip('<') + result)

    # find results using new uris
    new_prefixes = set(new_prefixes)
    for pattern in new_prefixes:
        results = re.findall(r"{}:([0-9|A-Za-z]+)".format(re.escape(pattern)), file_contents)
        for result in results:
            temp_list.append(pattern + ':' + result)

    # check if any placeholders
    if not temp_list:
        return 'No matching placeholders found'

    # fetch uids
    uids = get_uid(len(temp_list))
    uids = ["lincs:" + uid for uid in uids]
    mappings = dict(zip(temp_list, uids))

    # saving files in the data/minting directory
    dir_path = create_data_dir('mint_', '/code/app/data/minting')

    # save mappings file in the data/minting directory
    with open(dir_path + "/mappings.json", "w") as f:
        json.dump(mappings, f)

    # saving reference to response object
    with open(dir_path + "/mappings.json", 'r') as f:
        file_content = f.read()
    response = Response(file_content, media_type="json")

    # cleanup
    shutil.rmtree(dir_path)

    return response
