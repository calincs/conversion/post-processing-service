import os
import shutil

from fastapi import APIRouter, UploadFile, Form
from fastapi.responses import FileResponse

from starlette.background import BackgroundTask
from typing import Annotated

from app.utils.utils import create_data_dir, cleanup_files, get_rdf_suffix
from app.utils.graph_helpers import create_graph, serialize_graph


router = APIRouter()


@router.post("/", status_code=200)
@router.post("", status_code=200)
def combine(
	files: list[UploadFile],
	output_format: Annotated[str, Form(...)] = "turtle",
	encoding: Annotated[str, Form(...)] = "utf-8"):

	# output format for rdflib serialize can be [turtle, ttl, xml, pretty-xml, json-ld, ntriples, nt, n3, trig]
	if output_format not in ["turtle", "ttl", "xml", "pretty-xml", "json-ld", "ntriples", "nt", "n3", "trig"]:
		output_format = "turtle"

	# we store the input files until this job is complete
	# we store all validation results for this job until the job is complete
	output_path = create_data_dir("", "/code/app/data/combine/")
	output_filename = "combined" + get_rdf_suffix(output_format)
	output_file = output_path + output_filename

	input_files = {}
	for file in files:
		filename = file.filename
		input_temp_path = output_path + f"/input_{filename}"
		input_files[filename] = input_temp_path

		# temporarily save file to validate
		with open(input_temp_path, "wb") as buffer:
			shutil.copyfileobj(file.file, buffer)

	graph = create_graph(input_files)

	serialize_graph(graph, output_file, output_format)

	# Replace namespaces like "file:///code/app/data/combine/2023-11-04_08-25-25/
	# .replace(f"file://{output_path}/", "")
	with open(output_file, "r", encoding=encoding) as f_in:
		f_contents = f_in.read()
	f_contents = f_contents.replace(f"file://{output_path}/", "")
	with open(output_file, "w", encoding=encoding) as f_out:
		f_out.write(f_contents)

	# return the combined file and delete it from the container
	if os.path.isfile(output_file):
		return FileResponse(path=output_file, filename=output_filename, background=BackgroundTask(cleanup_files, output_path))
	else:
		return None
