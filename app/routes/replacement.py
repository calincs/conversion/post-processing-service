from fastapi import APIRouter, File, UploadFile
from app.utils.utils import create_data_dir, cleanup_files
from fastapi.responses import FileResponse
from starlette.background import BackgroundTask
from typing import Optional

import json
import re
import os


router = APIRouter()


"""
TODO: test more with regex patterns as input and update documentation for that

"""

@router.post("", status_code=200)
@router.post("/", status_code=200)
def replace(data_file: UploadFile = File(...), map_file: UploadFile = File(...), regex: bool = None, encoding: Optional[str] = "utf-8"):

    # handle missing file
    if not data_file:
        return "Please input data file"
    if not map_file:
        return "Please input mapping file"

    data_file_content = data_file.file.read().decode(encoding)
    map_file_content = map_file.file.read().decode(encoding)
    filename = data_file.filename
    output_path = create_data_dir("", "/code/app/data/replacements")
    output_file = output_path + "/" + filename

    # file format
    json_file = True
    try:
        mapping_file = json.loads(map_file_content)
    except:
        json_file = False

    if json_file:
        for data in mapping_file:
            # ignore blank lines
            if data:
                data_file_content = data_file_content.replace(data, mapping_file[data])
                data_file_content = data_file_content.replace('\r', '')  # remove extra white space
    else:
        mapping_file = map_file_content
        replacements = {}  # dict of mapping
        mapping_file_lines = mapping_file.split('\n')
        for mapping in mapping_file_lines:
            # ignore blank lines
            if mapping:
                map = []
                map = mapping.split('\t')
                if len(map) <= 1:
                    print('entity replacement "' + mapping + '" is invalid, please insert tab space between replacements')
                else:
                    replacements[map[0]] = map[1].strip('\r')
        # pattern replace
        for key in replacements:
            if regex:
                data_file_content = re.sub(key, replacements[key], data_file_content)
            else:
                # if the link to be replaced has characters like backslash, a match is not found. Thus needs to be replaced only for the non regex option.
                key_no_escp_chars = key.replace('\\', '')
                data_file_content = data_file_content.replace(key_no_escp_chars, replacements[key])

    with open(output_file, "w", encoding=encoding) as f:
        f.write(data_file_content)

    if os.path.isfile(output_file):
        return FileResponse(path=output_file, filename=filename, background=BackgroundTask(cleanup_files, output_path))
    else:
        return None
