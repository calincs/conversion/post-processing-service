from codecs import lookup_error
from fastapi import APIRouter
from .labels import router as label_router
from .coordinates import router as coordinate_router
#from .minting import router as minting_router
from .replacement import router as replacement_router
from .validate import router as validate_router
from .x3ml_conversion import router as conversion_router
#from .lookup import router as lookup_router
from .split import router as split_router
from .combine import router as combine_router
from .serialize import router as serialize_router
from .inverses import router as inverses_router

router = APIRouter()

router.include_router(
    label_router,
    prefix='/labels',
    tags=['labels'],
)

router.include_router(
    coordinate_router,
    prefix='/coordinates',
    tags=['coordinates'],
)

# router.include_router(
#     minting_router,
#     prefix='/mint',
#     tags=['minting'],
# )

router.include_router(
    replacement_router,
    prefix='/replacement',
    tags=['replacement'],
)

router.include_router(
    validate_router,
    prefix='/validate',
    tags=['validate'],
)

router.include_router(
    conversion_router,
    prefix='/conversion',
    tags=['x3ml conversion'],
)

# router.include_router(
#     lookup_router,
#     prefix='/lookup',
#     tags=['LINCS KG lookup'],
# )

router.include_router(
    split_router,
    prefix='/split',
    tags=['split RDF files'],
)

router.include_router(
    combine_router,
    prefix='/combine',
    tags=['combine'],
)

router.include_router(
    serialize_router,
    prefix='/serialize',
    tags=['RDF serialization'],
)

router.include_router(
    inverses_router,
    prefix='/inverses',
    tags=['Inverse properties'],
)
