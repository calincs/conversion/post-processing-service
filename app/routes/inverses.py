import os
import shutil

from fastapi import APIRouter, UploadFile, Form
from fastapi.responses import FileResponse
from rdflib import URIRef
from starlette.background import BackgroundTask
from typing import Annotated

from app.utils.utils import create_data_dir, cleanup_files
from app.utils.graph_helpers import create_graph, serialize_graph

router = APIRouter()


# format options listed here: https://jena.apache.org/documentation/io/
@router.post("", status_code=200)
@router.post("/", status_code=200)
def inverses(
		file: UploadFile,
		serialization: Annotated[str, Form(...)] = "",
		encoding: Annotated[str, Form(...)] = "utf-8"):

	if serialization not in ["turtle", "ttl", "xml", "pretty-xml", "json-ld", "ntriples", "nt", "n3", "trig"]:
		serialization = "turtle"

	data_path = create_data_dir("", "/code/app/data/inverses/")
	input_file = data_path + "/" + file.filename
	output_filename = "inversed_" + file.filename
	output_path = data_path + "/" + output_filename

	# we store the input file until this job is complete
	# we store all validation results for this job until the job is complete
	with open(input_file, "wb") as buffer:
		shutil.copyfileobj(file.file, buffer)

	inverse_mapping = {}
	with open("/code/app/data/inverses/inverse_properties.tsv", "r") as inverse_file:
		for line in inverse_file.readlines():
			split = line.strip().split("\t")
			inverse_mapping[split[0]] = split[1]

	g = create_graph({file.filename: input_file})
	for subj, pred, obj in g:
		if str(pred) in inverse_mapping:
			new_pred = URIRef(inverse_mapping[str(pred)])
			g.add((obj, new_pred, subj))
			g.remove((subj, pred, obj))

	serialize_graph(g, output_path, serialization)

	# Replace namespaces like "file:///code/app/data/combine/2023-11-04_08-25-25/
	# .replace(f"file://{output_path}/", "")
	with open(output_path, "r", encoding=encoding) as f_in:
		f_contents = f_in.read()
	f_contents = f_contents.replace(f"file://{output_path}/", "")
	with open(output_path, "w", encoding=encoding) as f_out:
		f_out.write(f_contents)

	# return the combined file and delete it from the container
	if os.path.isfile(output_path):
		return FileResponse(path=output_path, filename=file.filename, background=BackgroundTask(cleanup_files, data_path))

	else:
		return None
