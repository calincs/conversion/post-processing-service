from fastapi import APIRouter, File, Query as query
from rdflib import Graph, Literal, RDFS, URIRef, RDF
from typing import List, Optional
from app.utils.utils import get_ids, get_prefix, redirect_file, skip_owl, create_data_dir, geonames_response_body_chk, cleanup_files
from app.utils.label_queries import request_wiki_coordinates
from app.utils.redirects import Redirects
from starlette.background import BackgroundTask
from fastapi.responses import FileResponse

import json
import re
import requests
import os


"""
TODO
- update to use the LINCS geonames service
- confirm redirects are being used correctly
- could allow the user to choose the output serialization
"""


router = APIRouter()


@router.post("/geonames", status_code=200)
@router.post("/geonames/", status_code=200)
def get_geonames_coordinates(labels: bool = True, skip_predicate: List[str] = query(None), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    '''
    Fetch geonames coordinates

    Output should be of the form

    @prefix geonames: <https://sws.geonames.org/> .
    @prefix crm: <http://www.cidoc-crm.org/cidoc-crm/> .
    geonames:2077453 a crm:E53_Place ;
    crm:P168_place_is_defined_by    "POINT(-21.56667 114.91667)" .
    '''
    output_path = create_data_dir('geo_', '/code/app/data/coordinates')
    g = Graph()
    link = 'https://sws.geonames.org/'
    g.bind('geonames', 'https://sws.geonames.org/')
    g.bind('crm', 'http://www.cidoc-crm.org/cidoc-crm/')

    # handle missing file
    if not file[0]:
        return "Please input data file"

    file_content = file[0].decode(encoding)

    # get prefixes defined at top of file
    prefix = get_prefix(link, file_content)

    # extract ids matching chosen authority
    ids = get_ids(link, file_content, prefix, pattern='')

    # fetching redirects
    redirects = Redirects()
    redirects.new_file = file_content
    redirects.get_geonames_redirect(ids)

    # get updated redirected ids
    ids = get_ids(link, redirects.new_file, prefix, pattern='')

    # checking exempt uris
    exempt_ids = []
    if skip_predicate:
        predicates = skip_owl(skip_predicate)
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_content)))
            for label in label_text:
                exempt_ids += list(set(re.findall(r"geonames:([0-9]+)", label)))

    # removing exempt uris from list
    for exempt_id in exempt_ids:
        while (exempt_id in ids):
            ids.remove(exempt_id)

    for geo_id in ids:
        link = f"https://geonames.lincsproject.ca/geocode?id={geo_id}"
        req = requests.get(link)

        label = None
        lat = None
        lon = None

        if req.status_code == 200:
            data = req.json()
            if data.get("success"):
                for info in data["data"]:
                    if "properties" in info:
                        if "name" in info["properties"]:
                            label = info["properties"]["name"]
                    if "geometry" in info:
                        if "coordinates" in info["geometry"]:
                            [lon, lat] = info["geometry"]["coordinates"]

        # check if id exists
        if req.status_code == 404:
            continue

        s = URIRef('https://sws.geonames.org/{}'.format(geo_id))

        # Add label to graph
        if label:
            o = Literal(label, lang='en')
            g.add((s, RDFS.label, o))

        if lat and lon:
            try:
                # Add place definition to graph
                g.add((s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E53_Place")))

                # Add coordinates to graph
                coords = Literal("POINT({} {})".format(lon, lat))
                g.add((s, URIRef("http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by"), coords))
            except Exception as err:
                print("Failed to add coordinates: ", err)
        else:
            print("No coordinates found for : ", geo_id)

    redirect_file(redirects, output_path)

    missing_entities = geonames_response_body_chk(ids, 'https://sws.geonames.org/')
    non_existing_entities = {"non_existing_entities": missing_entities}

    if len(g) > 0:

        # saving files in the data/labels directory
        if missing_entities:
            with open(output_path + '/non_existing_entities.json', 'w') as f:
                json.dump(non_existing_entities, f)

        # checking for redirects
        redirect_file(redirects, output_path)

        # saving reference to response object
        output_file = output_path + "/geonames_coordinates.ttl"
        with open(output_file, "w", encoding=encoding) as f:
            f.write(g.serialize(format='turtle'))

        if os.path.isfile(output_file):
            return FileResponse(path=output_file, filename="geonames_coordinates.ttl", background=BackgroundTask(cleanup_files, output_path))
        else:
            return None
    else:
        if missing_entities:
            return non_existing_entities
        return {"error": "0 coordinates found for geonames"}


@router.post("/wikidata", status_code=200)
@router.post("/wikidata/", status_code=200)
def get_wikidata_coordinates(labels: bool = True, skip_predicate: List[str] = query(None), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    '''
    Fetch Wikidata coordinates

    Query of the form

    SELECT DISTINCT ?item ?coordinates
    WHERE {
      VALUES ?item {wd:Q210098 wd:Q2153758 wd:Q1129690}
      ?item wdt:P625 ?coordinates.
      SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
    }

    Output should be of the form

    @prefix wikidata: <http://www.wikidata.org/entity/> .
    @prefix crm: <http://www.cidoc-crm.org/cidoc-crm/> .
    wikidata:Q736742 a crm:E53_Place ;
    crm:P168_place_is_defined_by    "POINT(-0.2171 51.4235)" .
    '''

    output_path = create_data_dir('wiki_', '/code/app/data/coordinates')
    g = Graph()
    link = 'http://www.wikidata.org/entity/'
    g.bind('wikidata', link)
    g.bind('crm', 'http://www.cidoc-crm.org/cidoc-crm/')

    # handle missing file
    if not file[0]:
        return "Please input data file"

    file_content = file[0].decode(encoding)

    # get prefixes defined at top of file
    prefix = get_prefix(link, file_content)

    # extract ids matching chosen authority
    ids = get_ids(link, file_content, prefix, pattern='Q')

    # fetching redirects
    redirects = Redirects()
    redirects.new_file = file_content
    redirects.get_wikidata_redirect(ids)

    # get updated redirected ids
    ids = get_ids(link, redirects.new_file, prefix, pattern='Q')

    # checking exempt uris
    exempt_ids = []
    if skip_predicate:
        predicates = skip_owl(skip_predicate)
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_content)))
            for label in label_text:
                exempt_ids += list(set(re.findall(r"wikidata:(Q[0-9]+)", label)))

    # removing exempt uris from list
    for exempt_id in exempt_ids:
        while (exempt_id in ids):
            ids.remove(exempt_id)

    # fetch Wikidata coordinates
    request_wiki_coordinates(ids, g, output_path)

    if len(g) > 0:  # if any URIs exist

        # checking for redirects
        redirect_file(redirects, output_path)

        # saving reference to response object
        output_file = output_path + "/wikidata_coordinates.ttl"
        with open(output_file, "w", encoding=encoding) as f:
            f.write(g.serialize(format='turtle'))

        if os.path.isfile(output_file):
            return FileResponse(path=output_file, filename="wikidata_coordinates.ttl", background=BackgroundTask(cleanup_files, output_path))
        else:
            return None
    else:
        # TODO handle invalid entities
        # if missing_entities:
        #     return non_existing_entities
        return {"error": "0 coordinates found for Wikidata"}