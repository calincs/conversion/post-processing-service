from bs4 import BeautifulSoup
from fastapi import APIRouter, File, Query as query
from rdflib import Graph, Literal, RDFS, URIRef, RDF
from typing import List, Optional
from app.utils.utils import get_ids, get_prefix, redirect_file, return_file, skip_owl, create_data_dir, status_code_chk, cleanup_files
from app.utils.redirects import Redirects
from app.utils.label_queries import request_dbpedia_labels, isEnglish, request_loc_labels, request_getty_labels, Query, request_wiki_labels

import os
import re
import requests
import xml.etree.ElementTree as ET
import json


"""
TODO:
- add french support for more services. Particularly VIAF if possible. Getty is possible. And LINCSis possible. Currently only works for Wikidata and DBPedia
- If new errors come up with input files then switch to using rdflib to load the graphs instead of regex patterns. Particularly for LOC which is confusing with the pattern mappings.
- Make sure URIs are deduplicated before sending to authorities for labels
"""

router = APIRouter()


# to handle path with or without slash
@router.post("/wikidata", status_code=200)
@router.post("/wikidata/", status_code=200)
def get_wikidata_labels(lang_code: str = None, skip_predicate: List[str] = query(None), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    '''Fetch wikidata labels'''

    dir_path = create_data_dir('wikidata_', '/code/app/data/labels')

    g = Graph()
    link = 'http://www.wikidata.org/entity/'
    g.bind('wikidata', link)
    ids = []

    # handle missing file
    if not file[0]:
        return "Please input data file"
    file_contents = file[0].decode(encoding)

    # get prefixes defined at top of file
    prefix = get_prefix(link, file_contents)

    # extract ids matching chosen authority
    pattern = 'Q'
    ids += get_ids(link, file_contents, prefix, pattern)

    # fetching redirects
    redirects = Redirects()
    redirects.new_file = file_contents
    redirects.get_wikidata_redirect(ids)

    # get updated redirected ids
    alt_link = 'http://www.wikidata.org/prop/'
    ids = get_ids(link, redirects.new_file, prefix, pattern)
    ids += get_ids(alt_link, redirects.new_file, prefix, pattern)
    prop_ids = get_ids(alt_link, redirects.new_file, prefix, pattern='P')
    # We output any /prop labels if they exist in the input file but also warn the user that Wikidata properties
    # typically should not be used in LINCS data and that it's better to find an equivalent entity with a Q ID
    prop_warning = False
    if prop_ids:
        prop_warning = True

    # checking exempt uris
    exempt_ids = []
    if skip_predicate:
        predicates = skip_owl(skip_predicate)  # skip predicates in owl:sameAs
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_contents)))
            for label in label_text:
                exempt_ids += list(set(re.findall(r"[wikidata]:(Q[0-9]+)", label)))

    # removing exempt uris from list
    for id in exempt_ids:
        while (id in ids):
            ids.remove(id)

    # fetch wikidata labels
    request_wiki_labels(ids, g, dir_path, lang_code='en')

    # fetch wikidata labels in requested language code
    if lang_code:
        request_wiki_labels(ids, g, dir_path, lang_code)

    if len(g) > 0:  # if any URIs exist

        # checking for redirects
        redirect_file(redirects, dir_path)

        # saving reference to response object
        response = return_file(dir_path + "/wikidata_labels.ttl", dir_path, g, prop_warning=prop_warning)

        # cleanup
        cleanup_files(dir_path)

        return response
    else:
        if os.path.exists(dir_path + '/missing_labels_or_entities.json'):
            with open(dir_path + '/missing_labels_or_entities.json', 'r') as f:
                non_existing_entities = json.loads(f)
            return "0 labels found for wikidata +\n\n" + non_existing_entities
        return{"0 labels found for wikidata"}


@router.post("/lincs/", status_code=200)
@router.post("/lincs", status_code=200)
def get_lincs_labels(skip_predicate: List[str] = query(None), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    '''Fetch LINCS labels'''

    dir_path = create_data_dir('lincs_', '/code/app/data/labels')

    g = Graph()
    link = 'http://id.lincsproject.ca/'
    g.bind('lincs', link)
    ids = []

    # handle missing file
    if not file[0]:
        return "Please input data file"
    file_contents = file[0].decode(encoding)

    # get prefixes
    prefixes = re.findall(r'@prefix(.*?): <http://id.lincsproject.ca/> .', file_contents)
    for prefix in prefixes:
        prefix = prefix.strip(' ')
        ids += list(set(re.findall(r"{}:([0-9|A-Za-z]+)".format(re.escape(prefix)), file_contents)))
        ids += list(set(re.findall(r"{}([0-9|A-Za-z]+)".format(re.escape(link)), file_contents)))

    # checking exempt uris
    exempt_ids = []
    predicates = []
    if skip_predicate:
        predicates = skip_owl(skip_predicate)  # skip predicates in owl:sameAs
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_contents)))
            for label in label_text:
                exempt_ids += list(set(re.findall(r"{}:([0-9|A-Za-z]+)".format(re.escape(prefix)), label)))

    for id in exempt_ids:
        while (id in ids):
            ids.remove(id)

    non_existing_entities = {'missing_labels_or_entities': []}
    iter = 0
    for idx in range(0, len(ids), 50):
        end = idx + 50
        sub_ids = ids[(iter * 50):end]
        squery = '''PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                    PREFIX lincs: <http://id.lincsproject.ca/>
                    SELECT *
                    WHERE {
                    VALUES ?subject { ?ALL_VALUES }
                    ?subject rdfs:label ?label .
                }'''
        lincsquery = Query(endpoint="https://rs.lincsproject.ca/sparql", query=squery, authority='lincs')
        results = lincsquery.get_results(bulk_values=ids[idx:end])
        found_labels = []
        for result in results:
            try:
                found_labels.append(result['subject']['value'].rsplit('/', 1)[-1])
                g.add((
                    URIRef(result['subject']['value']),
                    RDFS.label,
                    Literal(result['label']['value'], lang=result['label']['xml:lang'])))
            except Exception as e:
                print('Error adding entity to graph')
                print(e)
        missing_labels = list(set(sub_ids) - set(found_labels))
        non_existing_entities['missing_labels_or_entities'] = non_existing_entities['missing_labels_or_entities'] + missing_labels

    if non_existing_entities['missing_labels_or_entities']:
        with open(dir_path + '/missing_labels_or_entities.json', 'w') as f:
            json.dump(non_existing_entities, f)

    if len(g) > 0:
        # saving reference to response object
        response = return_file(dir_path + "/lincs_labels.ttl", dir_path, g, encoding=encoding)

        # cleanup
        cleanup_files(dir_path)

        return response
    else:
        # even if no entities were found, inform user of non existing labels in their input file
        if os.path.exists('missing_labels_or_entities.json'):
            return "0 labels found for lincs +\n\n" + non_existing_entities
        return{"0 labels found for lincs"}


@router.post("/dbpedia", status_code=200)
@router.post("/dbpedia/", status_code=200)
def get_dbpedia_labels(lang_code: str = None, skip_predicate: List[str] = query(None), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    '''Fetch dbpedia labels'''
    dir_path = create_data_dir('dbpedia_', '/code/app/data/labels')

    g = Graph()
    g.bind('dbr', 'http://dbpedia.org/resource/')
    ids = []

    # handle missing file
    if not file[0]:
        return "Please input data file"
    file_contents = file[0].decode(encoding)

    prefixes = re.findall(r'@prefix(.*?): <http://dbpedia.org/resource/> .', file_contents)
    for prefix in prefixes:
        # extract ids matching chosen authority
        ids += list(set(re.findall(r"[{}]:([0-9|A-Za-z]+)".format(re.escape(prefix)), file_contents)))
        # if ids list is empty
        if not ids:
            ids += list(set(re.findall(r"http://dbpedia.org/resource/([0-9|A-Za-z]+)", file_contents)))

        # fetching redirects
    redirects = Redirects()
    redirects.new_file = file_contents
    redirects.get_dbpedia_redirect(ids)

    for prefix in prefixes:
        # get updated redirected ids
        ids += list(set(re.findall(r"[{}]:([0-9|A-Za-z]+)".format(re.escape(prefix)), redirects.new_file)))
        # if ids list is empty
        if not ids:
            ids += list(set(re.findall(r"http://dbpedia.org/resource/([0-9|A-Za-z]+)", redirects.new_file)))

    # checking exempt uris
    exempt_ids = []
    if skip_predicate:
        predicates = skip_owl(skip_predicate)
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_contents)))
            for label in label_text:
                exempt_ids += list(set(re.findall(r"dbr:([0-9|A-Za-z]+)", label)))

    # removing exempt uris from list
    for id in exempt_ids:
        while (id in ids):
            ids.remove(id)

    # fetch dbpedia labels
    request_dbpedia_labels(ids, g, dir_path, lang_code='en')

    # fetch dbpedia labels in requested language code
    if lang_code:
        request_dbpedia_labels(ids, g, dir_path, lang_code)

    if len(g) > 0:
        # checking for redirects
        redirect_file(redirects, dir_path)

        # saving reference to response object
        response = return_file(dir_path + "/dbpedia_labels.ttl", dir_path, g, encoding=encoding)

        # cleanup
        cleanup_files(dir_path)

        return response
    else:
        if os.path.exists(dir_path + '/missing_labels_or_entities.json'):
            with open(dir_path + '/missing_labels_or_entities.json', 'r') as f:
                non_existing_entities = json.loads(f)
            return "0 labels found for dbpedia +\n\n" + non_existing_entities
        return{"0 labels found for dbpedia"}


@router.post("/viaf", status_code=200)
@router.post("/viaf/", status_code=200)
def get_viaf_labels(skip_predicate: List[str] = query(None), file: List[bytes] = File(...), date: bool = None, encoding: Optional[str] = "utf-8"):
    '''
    Fetch viaf labels
    person labels will have their name swapped to be first name last name instead of last name, first name
    '''
    dir_path = create_data_dir('viaf_', '/code/app/data/labels')

    g = Graph()
    link = 'http://viaf.org/viaf/'
    g.bind('viaf', link)
    ns = {'v': 'http://viaf.org/viaf/terms#', 'rd': 'http://viaf.org/viaf/abandonedViafRecord'}
    ids = []

    # handle missing file
    if not file[0]:
        return "Please input data file"
    file_contents = file[0].decode(encoding)

    # get prefixes defined at top of file

    prefix = get_prefix(link, file_contents)
    # extract ids matching chosen authority
    ids += get_ids(link, file_contents, prefix, pattern='')
    # fetching redirects
    redirects = Redirects()
    redirects.new_file = file_contents
    redirects.get_viaf_redirect(ids)

    # get updated redirected ids
    ids = get_ids(link, redirects.new_file, prefix, pattern='')

    # checking exempt uris
    exempt_ids = []
    if skip_predicate:
        predicates = skip_owl(skip_predicate)
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_contents)))
            for label in label_text:
                exempt_ids += list(set(re.findall(r"viaf:([0-9]+)", label)))

    # removing exempt uris from list
    for id in exempt_ids:
        while (id in ids):
            ids.remove(id)

    missing_entities = []
    # extracting viaf labels from .xml webpage
    for id in list(set(ids)):
        response = requests.get("http://www.viaf.org/viaf/{}/viaf.xml".format(id))
        if not status_code_chk(response):
            missing_entities.append("viaf: {}".format(id))
            continue
        root = ET.fromstring(response.text)
        all_data_nodes = root.findall('v:mainHeadings/v:data', ns)

        # Check if this entity refers to a person or another entity type.
        # If it's a person we will do cleaning on the label to change the name order
        entity_type = root.find('v:nameType', ns)
        try:
            entity_type = entity_type.text
        except Exception:
            entity_type = ""

        # Find the labels
        most_sources = 0
        most_sources_w_date = 0
        name_wo_date = ''
        name_w_date = ''
        for node in all_data_nodes:
            name = node.find('v:text', ns)

            if name is not None:
                name = name.text
            has_year = any([char.isdigit() for char in name])
            if has_year:
                num_sources_w_date = len(node.findall('v:sources/v:s', ns))
                num_sources = 0
            else:
                num_sources_w_date = 0
                num_sources = len(node.findall('v:sources/v:s', ns))

            if num_sources > most_sources:
                most_sources = num_sources
                name_wo_date = name

            if num_sources_w_date > most_sources_w_date:
                most_sources_w_date = num_sources_w_date
                name_w_date = name

        if name_w_date == '':
            if name_wo_date == '':
                print("no name found for {}".format(id))
            # only splitting if there is a comma in the literal, assuming that means its a name
            split_name = []
            if "," in name_wo_date:
                if entity_type == "Personal":
                    combined_string = ""
                    split_name = name_wo_date.split(',')
                    for i in range(len(split_name) - 1, -1, -1):
                        combined_string = combined_string + " " + split_name[i].strip()
                    name_wo_date = combined_string.strip()
            if isEnglish(name_wo_date):
                o = Literal(name_wo_date, lang='en')
            else:
                o = Literal(name_wo_date)
        else:
            split_name = []
            if entity_type == "Personal":
                if "," in name_w_date:
                    combined_string = ""
                    split_name = name_w_date.split(',')
                    for i in range(len(split_name) - 2, -1, -1):
                        combined_string = combined_string + " " + split_name[i].strip()
                        name_w_date = combined_string.strip()
                    if date:
                        # the last element in split_name is the date, only add that if the parameter for date is true
                        name_w_date = (combined_string + ", " + split_name[-1].strip()).strip()

            if isEnglish(name_w_date):
                o = Literal(name_w_date, lang='en')
            else:
                o = Literal(name_w_date)
        s = URIRef('http://viaf.org/viaf/{}'.format(id))
        g.add((s, RDFS.label, o))

    non_existing_entities = {"missing_labels_or_entities": missing_entities}

    if len(g) > 0:

        if missing_entities:
            with open(dir_path + '/missing_labels_or_entities.json', 'w') as f:
                json.dump(non_existing_entities, f)
        # checking for redirects
        redirect_file(redirects, dir_path)

        # saving reference to response object
        response = return_file(dir_path + "/viaf_labels.ttl", dir_path, g, encoding=encoding)

        # cleanup
        cleanup_files(dir_path)

        return response
    else:
        if missing_entities:
            return "0 labels found for VIAF +\n\n" + non_existing_entities
        return{"0 labels found for VIAF"}


@router.post("/loc", status_code=200)
@router.post("/loc/", status_code=200)
def get_loc_labels(skip_predicate: List[str] = query(None), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    dir_path = create_data_dir('loc_', '/code/app/data/labels')
    # When adding a new service, define it in the dictionary below with correct key and values.
    # the key is the prefix.
    # if no pattern is required set patterns to [None]. This is important as simply leaving it as None would give an error.
    # Patterns is any pattern that the uris begin with. Example /names/ uris are like n113456.
    # For other parameters need to look at the html file for a loc uri of that type

    service_dict = {
        'locworks': {
            'link': 'http://id.loc.gov/resources/works/',
            'patterns': [""],
            'end': '" .',
            'num': 2,
            'rdf_pattern': '> <http://www.w3.org/2000/01/rdf-schema#label>'},
        'locnames': {
            'link': 'http://id.loc.gov/authorities/names/',
            'patterns': ['n', 'no', 'nr'],
            "end": '" .',
            "num": 2,
            "rdf_pattern": '> <http://www.loc.gov/mads/rdf/v1#authoritativeLabel>'},
        'locsubjects': {
            'link': 'http://id.loc.gov/authorities/subjects/',
            'patterns': ['sh'],
            "end": '@',
            "num": 0,
            "rdf_pattern": '> <http://www.loc.gov/mads/rdf/v1#authoritativeLabel>'},
        'locinsts': {
            'link': 'http://id.loc.gov/resources/instances/',
            'patterns': [""],
            "end": '" .',
            "num": 2,
            "rdf_pattern": '<http://id.loc.gov/ontologies/bibframe/mainTitle>'},
        'locprovs': {
            'link': 'http://id.loc.gov/entities/providers/',
            'patterns': [None],
            "end": '" .',
            "num": 2,
            "rdf_pattern": '> <http://www.loc.gov/mads/rdf/v1#authoritativeLabel>'},
        'loclangs': {
            'link': 'http://id.loc.gov/vocabulary/languages/',
            'patterns': [None],
            "end": '@',
            "num": 0,
            "rdf_pattern": '> <http://www.loc.gov/mads/rdf/v1#authoritativeLabel>'},
        'locgenreForms': {
            'link': 'http://id.loc.gov/authorities/genreForms/',
            'patterns': ['gf'],
            "end": '@',
            "num": 0,
            "rdf_pattern": '> <http://www.loc.gov/mads/rdf/v1#authoritativeLabel>'},
        'locmaudience': {
            'link': 'http://id.loc.gov/vocabulary/maudience/',
            'patterns': [None],
            "end": '" .',
            "num": 2,
            "rdf_pattern": '> <http://www.loc.gov/mads/rdf/v1#authoritativeLabel>'}
    }
    g = Graph()
    # handle missing file
    if not file[0]:
        return "Please input data file"

    file_content = file[0].decode(encoding)

    # checking exempt uris
    exempt_ids = []

    if skip_predicate:
        predicates = skip_owl(skip_predicate)
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_content)))
            for label in label_text:
                exempt_ids += list(set(re.findall(r"locworks:([0-9]+)", label)))
                exempt_ids += list(set(re.findall(r"locnames:([n|nr|no[0-9]+)", label)))
                exempt_ids += list(set(re.findall(r"locsubjects:(sh[0-9]+)", label)))
                exempt_ids += list(set(re.findall(r"locprovs:(e[0-9|A-Za-z]+)", label)))
                exempt_ids += list(set(re.findall(r"locinsts:([0-9]+)", label)))
                # why no l ids?
                exempt_ids += list(set(re.findall(r"locgenreForms:(gf[0-9]+)", label)))

    redirects = Redirects()
    missing_entities = []
    # this loop handles all LOC services defined in the service_dict.
    for service in service_dict:
        link = service_dict[service]['link']
        g.bind(service, link)
        ids = []
        prefix = get_prefix(link, file_content)
        patterns = service_dict[service]['patterns']
        # locprovs,loclangs,locmaudience services work a little differently than the rest, thus have
        # different code associated with them for finding labels and redirects.
        # for future services with pattern = ['None'], will need to have different functionality as well.
        for pattern in patterns:
            if service == 'locprovs' and prefix is not None:
                ids += list(set(re.findall(r"[{}]:(e[0-9|A-Za-z]+)".format(re.escape(prefix)), file_content)))
            elif (service == 'loclangs' or service == 'locmaudience') and prefix is not None:
                ids += list(set(re.findall(r"{}:([0-9|A-Za-z]+)".format(re.escape(prefix)), file_content)))
            else:
                if file_content is not None and pattern is not None:
                    ids += get_ids(link, file_content, prefix, pattern=pattern)
        redirects.get_loc_redirects(service + ":", link, ids)

        for pattern in patterns:
            if service == 'locprovs' and prefix and redirects.new_file:
                ids += list(set(re.findall(r"[{}]:(e[0-9|A-Za-z]+)".format(re.escape(prefix)), redirects.new_file)))
            elif (service == 'loclangs' or service == 'locmaudience') and prefix and redirects.new_file:
                ids += list(set(re.findall(r"{}:([0-9|A-Za-z]+)".format(re.escape(prefix)), redirects.new_file)))
            else:
                if redirects.new_file is not None and pattern is not None:
                    ids += get_ids(link, redirects.new_file, prefix, pattern=pattern)
        end = service_dict[service]['end']
        num = service_dict[service]['num']
        rdf_pattern = service_dict[service]['rdf_pattern']
        missing_entities += request_loc_labels(ids, exempt_ids, link, g, end=end, num=num, rdf_pattern=rdf_pattern)

    missing_entities = list(set(missing_entities))

    non_existing_entities = {"missing_labels_or_entities": missing_entities}

    if len(g) > 0:  # if any labels exist

        if missing_entities:
            with open(dir_path + '/missing_labels_or_entities.json', 'w') as f:
                json.dump(non_existing_entities, f)
        # checking for redirects
        redirect_file(redirects, dir_path)

        # saving reference to response object
        response = return_file(dir_path + "/loc_labels.ttl", dir_path, g, encoding=encoding)

        # cleanup
        cleanup_files(dir_path)

        return response
    else:
        if missing_entities:
            return "0 labels found for loc +\n\n" + str(non_existing_entities)
        return{"0 labels found for loc"}


@router.post("/geonames", status_code=200)
@router.post("/geonames/", status_code=200)
def get_geonames_labels(skip_predicate: List[str] = query(None), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    '''Fetch geonames labels'''
    dir_path = create_data_dir('geo_', '/code/app/data/labels')

    g = Graph()
    link = 'https://sws.geonames.org/'
    g.bind('geonames', link)

    # handle missing file
    if not file[0]:
        return "Please input data file"
    file_content = file[0].decode(encoding)

    # get prefixes defined at top of file

    prefix = get_prefix(link, file_content)

    # extract ids matching chosen authority
    ids = get_ids(link, file_content, prefix, pattern='')

    # fetching redirects
    redirects = Redirects()
    redirects.new_file = file_content
    redirects.get_geonames_redirect(ids)

    # get updated redirected ids
    ids = get_ids(link, redirects.new_file, prefix, pattern='')

    # checking exempt uris
    exempt_ids = []
    if skip_predicate:
        predicates = skip_owl(skip_predicate)
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_content)))
            for label in label_text:
                exempt_ids += list(set(re.findall(r"geonames:([0-9]+)", label)))

    # removing exempt uris from list
    for id in exempt_ids:
        while (id in ids):
            ids.remove(id)

    missing_entities = []
    for id in ids:
        link = f"https://geonames.lincsproject.ca/geocode?id={id}"
        req = requests.get(link)
        label = None

        # If connection found, retrieve data 
        if req.status_code == 200:
            data = req.json() 

            if data.get("success"):
                for info in data["data"]:
                    if "properties" in info:
                        if "name" in info["properties"]:
                            label = info["properties"]["name"]

        if req.status_code == '404' or req.status_code == '500' or label == None:
            missing_entities.append("geonames:" + id)
            continue

        if label:
            o = Literal(label, lang='en')
            s = URIRef(f'https://sws.geonames.org/{id}')
            g.add((s, RDFS.label, o))

    redirect_file(redirects, dir_path)

    non_existing_entities = {"missing_labels_or_entities": missing_entities}

    if len(g) > 0:

        if missing_entities:
            with open(dir_path + '/missing_labels_or_entities.json', 'w') as f:
                json.dump(non_existing_entities, f)
        # checking for redirects
        redirect_file(redirects, dir_path)

        # saving reference to response object
        response = return_file(dir_path + "/geonames_labels.ttl", dir_path, g, encoding=encoding)

        # cleanup
        cleanup_files(dir_path)

        return response
    else:
        if missing_entities:
            return "0 labels found for geonames +\n\n" + non_existing_entities
        return{"0 labels found for geonames"}


@router.post("/getty", status_code=200)
@router.post("/getty/", status_code=200)
def get_getty_labels(skip_predicate: List[str] = query(None), file: List[bytes] = File(...), encoding: Optional[str] = "utf-8"):
    '''Fetch getty labels'''

    dir_path = create_data_dir('getty_', '/code/app/data/labels')

    a_link = 'http://vocab.getty.edu/aat/'
    #c_link = 'http://vocab.getty.edu/cona/'
    t_link = 'http://vocab.getty.edu/tgn/'
    u_link = 'http://vocab.getty.edu/ulan/'
    g = Graph()

    # one list for each getty dataset
    a_ids, t_ids, u_ids = [], [], []

    # handle missing file
    if not file[0]:
        return "Please input data file"
    file_content = file[0].decode(encoding)

    # get prefixes defined at top of file
    a_prefix = get_prefix(a_link, file_content)
    #c_prefix = get_prefix(c_link, file_content)
    t_prefix = get_prefix(t_link, file_content)
    u_prefix = get_prefix(u_link, file_content)

    # **** extract ids matching chosen authority: aat****
    a_ids += get_ids(a_link, file_content, a_prefix, pattern='')
    # fetching redirects
    redirects = Redirects()
    redirects.new_file = file_content
    redirects.get_getty_aat_redirect(a_ids)

    # get updated redirected ids
    a_ids = get_ids(a_link, redirects.new_file, a_prefix, pattern='')

    # **** extract ids matching chosen authority: cona****
    #c_ids += get_ids(c_link, file_content, c_prefix, pattern='')

    # fetching redirects
    #redirects.get_getty_cona_redirect(c_ids)

    # get updated redirected ids
    #c_ids = get_ids(c_link, redirects.new_file, c_prefix, pattern='')

    # **** extract ids matching chosen authority: tgn****
    t_ids += get_ids(t_link, file_content, t_prefix, pattern='')

    # fetching redirects
    redirects.get_getty_tgn_redirect(t_ids)

    # get updated redirected ids
    t_ids = get_ids(t_link, redirects.new_file, t_prefix, pattern='')

    # **** extract ids matching chosen authority: ulan****
    u_ids += get_ids(u_link, file_content, u_prefix, pattern='')

    # fetching redirects
    redirects.get_getty_ulan_redirect(u_ids)

    # get updated redirected ids
    u_ids = get_ids(u_link, redirects.new_file, u_prefix, pattern='')

    # checking exempt uris
    exempt_ids = []
    if skip_predicate:
        predicates = skip_owl(skip_predicate)
        for predicate in predicates:
            label_text = list(set(re.findall(r'{}(.*)'.format(re.escape(predicate.strip())), file_content)))
            for label in label_text:
                prefixes = [a_prefix, t_prefix, u_prefix]
                for prefix in prefixes:
                    if prefix:
                        exempt_ids += list(set(re.findall(r"{}:([0-9]+)".format(re.escape(prefix)), label)))

    missing_entities = []
    # fetching labels with get request
    # used .find as faster than regex in this case

    # AAT dataset
    link = 'http://vocab.getty.edu/aat/'
    missing_entities += request_getty_labels(a_ids, exempt_ids, link, g)

    # # CONA dataset
    # for id in c_ids:
    #     if id in exempt_ids:
    #         continue
    #     request_link = "http://vocab.getty.edu/page/cona/" + id
    #     req = requests.get(request_link)
    #     if not status_code_chk(req):
    #         missing_entities.append("http://vocab.getty.edu/page/cona/" + id)
    #         continue
    #     soup = BeautifulSoup(req.text, 'html.parser')
    #     if soup:
    #         label = soup.find_all('b')[2].string
    #         o = Literal(label, lang='en')
    #         s = URIRef(request_link)
    #         g.add((s, RDFS.label, o))

    # TGN dataset
    link = 'http://vocab.getty.edu/tgn/'
    missing_entities += request_getty_labels(t_ids, exempt_ids, link, g)

    # ULAN dataset
    link = 'http://vocab.getty.edu/ulan/'
    missing_entities += request_getty_labels(u_ids, exempt_ids, link, g)

    redirect_file(redirects, dir_path)

    non_existing_entities = {"missing_labels_or_entities": missing_entities}

    # return response body as a file
    if len(g) > 0:

        with open(dir_path + '/missing_labels_or_entities.json', 'w') as f:
            json.dump(non_existing_entities, f)

        # checking for redirects
        redirect_file(redirects, dir_path)

        # saving reference to response object
        response = return_file(dir_path + "/getty_labels.ttl", dir_path, g, encoding=encoding)

        # cleanup
        cleanup_files(dir_path)

        return response
    else:
        if missing_entities:
            return "0 labels found for getty +\n\n" + non_existing_entities
        return{"0 labels found for getty"}
