'''
Purpose : 3M conversion without interacting with the GUI.
Two endpoints provided : /custom and /diffbot.
Runs conversion command from console.
Refer: https://github.com/isl/x3ml#run-from-console
'''
from typing import List, Optional
from fastapi import APIRouter, File, Form, HTTPException
from starlette.responses import Response
from starlette.background import BackgroundTask
from fastapi.responses import FileResponse
from app.utils.utils import create_data_dir, cleanup_files

import requests
import os
import sys
import subprocess
import io
import zipfile
import shutil

router = APIRouter()


def return_files(filename, file_path, cleanup_folder, headers={}):
	'''
	Creates a response object with the specified body.
	Parameters:
		file_patj(str) : Path to the .ttl output file from 3M
	Returns:
		resp: Response object
	'''
	if os.path.isfile(file_path):
		return FileResponse(path=file_path, filename=filename, background=BackgroundTask(cleanup_files, cleanup_folder), headers=headers)
	else:
		return None


def input_validation(file_links, input_file, map_file, policy_file):
	'''
	Validates input files necessary for program to run.
	Parameters:
		file_links (str) : JSON format with file urls
		input_file (str) : Input file
		map_file (str) : Mapping file
		policy_file (str) : Generator/Policy file
	'''
	if(file_links is None and input_file is None and map_file is None and policy_file is None):
		raise HTTPException(status_code=400, detail="No Input Files Provided")
	if(file_links is None and input_file is None):
		raise HTTPException(status_code=400, detail="Input File Missing")
	if(file_links is None and map_file is None):
		raise HTTPException(status_code=400, detail="Mapping File Missing")
	if(file_links is None and policy_file is None):
		raise HTTPException(status_code=400, detail="Policy File Missing")


@router.post("/x3ml/custom/", status_code=200)
@router.post("/x3ml/custom", status_code=200)
def custom_conversion(
		input_file: Optional[List[bytes]] = File(None),
		map_file: Optional[List[bytes]] = File(None),
		policy_file: Optional[List[bytes]] = File(None),
		output_file: str = 'x3ml_output.ttl',
		format_output_file: str = 'text/turtle',
		uuid_test_size: str = '2',
		encoding: Optional[str] = "utf-8",
		file_links: Optional[str] = Form(None)):
	'''
	Function handling the custom endpoint.
	'''
	input_validation(file_links, input_file, map_file, policy_file)

	# uuid_test_size should not be a negative value, setting it to default value of 2.
	if int(uuid_test_size) < 0:
		uuid_test_size = 2

	# the original working directory path
	in_out_path = '/code/app/data/x3ml_conversion'

	# getting filepath of the jar file
	jar_file_path = os.path.join(in_out_path, 'x3ml-engine-2.2.0-exejar.jar')

	output_path = create_data_dir("custom_", "/code/app/data/x3ml_conversion/")

	# String with names of input files which will be supplied to the java command.
	input_files_string, map_files_string = "", ""
	file_links_json = {}

	# if files were provided as links
	if file_links is not None:

		# converting string to dictionary
		file_links_json = eval(file_links)

		# extracting file links
		# Initialized with @ to match x3ml format of providing files as urls.
		# Refer: https://github.com/isl/x3ml#run-from-console
		input_files_string = '@'

		# File links are provided in JSON format.
		# Example can be found in example_usage/lincs_post_processing_api.ipynb
		# under the 3M conversion section.
		file_links_list = file_links_json["request"]["input_file"]

		# input file names are separated by a comma in the command
		for link in file_links_list:
			input_files_string = input_files_string + link["input_file_link"] + ","

		# similar process of getting mapping files' urls
		file_links_list = file_links_json["request"]["map_file"]
		file_num = 1
		for link in file_links_list:
			# getting content of mapping files from urls
			r = requests.get(link["map_file_link"])
			map_file_content = r.text
			# saving content gotten from urls as files on system.
			with open(output_path + "/map_file{}.x3ml".format(file_num), 'w') as f:
				for line in map_file_content:
					f.write(line)
			# constructing string with mapping file names for java command.
			map_files_string = map_files_string + output_path + "/map_file{}.x3ml".format(file_num) + ","
			file_num = file_num + 1

		# similarly, getting content of policy file and storing on user system.
		policy_file = file_links_json["request"]["policy_file"]
		r = requests.get(policy_file)
		policy_file_content = r.text
		with open(output_path + "/policy_file.xml", 'w') as f:
			for line in policy_file_content:
				f.write(line)

		# removing last unnecessary comma
		input_files_string = input_files_string[:-1]
		map_files_string = map_files_string[:-1]

	# if the input files were not provided as links, they could be single or multiple file uploads
	else:
		file_num = 1

		# Getting file content from uploads and writing them into new files on the system
		for file in input_file:
			with open(output_path + "/input_file{}.xml".format(file_num), 'w') as f:
				f.write(file.decode(encoding))
			input_files_string = input_files_string + output_path + "/input_file{}.xml,".format(file_num)
			file_num = file_num + 1

		# removing the last comma
		input_files_string = input_files_string[:-1]

		# could be single or multiple mapping files.
		file_num = 1
		for file in map_file:
			with open(output_path + "/map_file{}.x3ml".format(file_num), 'w') as f:
				f.write(file.decode(encoding))
			map_files_string = map_files_string + output_path + "/map_file{}.x3ml,".format(file_num)
			file_num = file_num + 1

		# removing the last comma
		map_files_string = map_files_string[:-1]

		# single policy file
		for file in policy_file:
			with open(output_path + "/policy_file.xml", 'w') as f:
				f.write(file.decode(encoding))

	# running java command that saves output file in x3ml_conversion directory
	proc = subprocess.run(["java", "-jar", jar_file_path, "-i", input_files_string, "-x", map_files_string, "-p", f"{output_path}/policy_file.xml", "-o", output_path + "/" + output_file, "-f", format_output_file, "-u", uuid_test_size], stdout=open(output_path + "/x3ml_log.txt", "w"))

	# Saving reference to response object
	zip_filename = "x3ml_output.zip"
	s = io.BytesIO()
	zf = zipfile.ZipFile(s, "w")

	for file in [output_file, "x3ml_log.txt"]:
		try:
			zf.write(output_path + "/" + file, file)
		except FileNotFoundError as err:
			# TODO handle this error better. It probably means that 3M failed to convert.
			print(err)

	# Must close zip for all contents to be written
	zf.close()

	# Grab ZIP file from in-memory, make response with correct MIME-type
	resp = Response(s.getvalue(), media_type="application/x-zip-compressed", headers={
		'Content-Disposition': f'attachment;filename={zip_filename}'
	})

	# cleanup
	shutil.rmtree(output_path)
	return resp


@router.post("/x3ml/person/", status_code=200)
@router.post("/x3ml/person", status_code=200)
def person_conversion(
		input_file: Optional[List[bytes]] = File(None),
		output_file: str = 'x3ml_output.ttl',
		format_output_file: str = 'text/turtle',
		uuid_test_size: str = '2',
		encoding: Optional[str] = "utf-8"):

	in_out_path = '/code/app/data/x3ml_conversion'

	map_file = os.path.join(in_out_path, 'person/3m-x3ml-output.x3ml')
	policy_file = os.path.join(in_out_path, 'person/generator-policy.xml')

	# uuid_test_size should not be a negative value, setting it to default value of 2.
	if int(uuid_test_size) < 0:
		uuid_test_size = 2

	# getting filepath of the jar file
	jar_file_path = os.path.join(in_out_path, 'x3ml-engine-2.2.0-exejar.jar')

	output_path = create_data_dir("custom_", "/code/app/data/x3ml_conversion/")

	# String with names of input files which will be supplied to the java command.
	input_files_string = ""

	# Getting file content from uploads and writing them into new files on the system
	file_num = 1
	for file in input_file:
		with open(output_path + "/input_file{}.xml".format(file_num), 'w') as f:
			f.write(file.decode(encoding))
		input_files_string = input_files_string + output_path + "/input_file{}.xml,".format(file_num)
		file_num = file_num + 1

	# removing the last comma
	input_files_string = input_files_string[:-1]

	# running java command that saves output file in x3ml_conversion directory
	proc = subprocess.run(["java", "-jar", jar_file_path, "-i", input_files_string, "-x", map_file, "-p", policy_file, "-o", output_path + "/" + output_file, "-f", format_output_file, "-u", uuid_test_size], stdout=open(output_path + "/x3ml_log.txt", "w"))

	# Saving reference to response object
	zip_filename = "x3ml_output.zip"
	s = io.BytesIO()
	zf = zipfile.ZipFile(s, "w")

	for file in [output_file, "x3ml_log.txt"]:
		try:
			zf.write(output_path + "/" + file, file)
		except FileNotFoundError as err:
			# TODO handle this error better. It probably means that 3M failed to convert.
			print(err)

	# Must close zip for all contents to be written
	zf.close()

	# Grab ZIP file from in-memory, make response with correct MIME-type
	resp = Response(s.getvalue(), media_type="application/x-zip-compressed", headers={
		'Content-Disposition': f'attachment;filename={zip_filename}'
	})

	# cleanup
	shutil.rmtree(output_path)
	return resp
