import io
import json
import os
import shutil
import zipfile

from app.utils.label_queries import Query
from app.utils.utils import create_data_dir
from fastapi import APIRouter, File
from rdflib import Graph, URIRef
from starlette.responses import Response
from typing import Optional


router = APIRouter()

"""
TODO:
- this needs to be tested and fixed. For now removed from postman examples and service.

"""


def lookup_lincs_kg(ids):
    new_uris = {}

    # check for subjects in KG
    for idx in range(0, len(ids), 50):
        end = idx + 50
        squery = '''PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                    PREFIX lincs: <http://id.lincsproject.ca/>
                    PREFIX owl:<http://www.w3.org/2002/07/owl#>
                    PREFIX wikidata:<http://www.wikidata.org/entity/>
                    SELECT ?subject ?object
                    WHERE {
                    VALUES ?object { ?ALL_VALUES }
                    ?subject owl:sameAs ?object .
                }'''
        lincsquery = Query(endpoint="https://rs.stage.lincsproject.ca/sparql", query=squery, authority='none')
        results = lincsquery.get_results(bulk_values=ids[idx:end])

        if results is None:
            # check for objects in KG

            for idx in range(0, len(ids), 50):
                end = idx + 50
                squery = '''PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                            PREFIX lincs: <http://id.lincsproject.ca/>
                            PREFIX owl:<http://www.w3.org/2002/07/owl#>
                            PREFIX wikidata:<http://www.wikidata.org/entity/>
                            SELECT ?subject ?object
                            WHERE {
                            VALUES ?subject { ?ALL_VALUES }
                            ?subject owl:sameAs ?object .
                        }'''
                lincsquery = Query(endpoint="https://rs.stage.lincsproject.ca/sparql", query=squery, authority='none')
                results = lincsquery.get_results(bulk_values=ids[idx:end])
                if results is None:
                    return 'No replacements present in LINCS KG'
        for result in results:
            try:
                new_uris[result['object']['value']] = result['subject']['value']

            except Exception as e:
                print('Error adding entity to graph: ', e)
    return new_uris


def return_lookup_file(dir_path):
    # Zipping files test
    zip_filename = "LINCS_KG_Lookup.zip"
    s = io.BytesIO()
    zf = zipfile.ZipFile(s, "w")

    # clean json file
    with open(dir_path + "/lookup_mappings.json", "r") as f:
        fc = f.read()
    with open(dir_path + "/lookup_mappings.json", "w") as f:
        fc = fc.replace('http://www.wikidata.org/entity/', 'wikidata:')
        fc = fc.replace('http://viaf.org/viaf/', 'viaf:')
        fc = fc.replace('http://id.lincsproject.ca/', 'lincs:')
        f.write(fc)

    # Add file, at correct path
    zf.write(dir_path + '/lookup_mappings.json', 'lookup_mappings.json')
    if os.path.exists(dir_path + '/owlsameAs.ttl'):
        zf.write(dir_path + '/owlsameAs.ttl', 'owlsameAs.ttl')

    # Must close zip for all contents to be written
    zf.close()

    # Grab ZIP file from in-memory, make response with correct MIME-type
    resp = Response(s.getvalue(), media_type="application/x-zip-compressed", headers={
        'Content-Disposition': f'attachment;filename={zip_filename}'
    })

    return resp


@router.post("/", status_code=200)
@router.post("", status_code=200)
def kg_lookup(file: bytes = File(...), encoding: Optional[str] = "utf-8"):
    # parse rdf file
    g = Graph()
    g.parse(file)

    # extract uris
    uris = []
    owl = {}

    for subj, pred, obj in g:
        # ignore owl:sameAs
        if 'http' not in obj:
            continue

        if str(pred) != 'http://www.w3.org/2002/07/owl#sameAs':
            uris.append('<' + str(subj) + '>')
            uris.append('<' + str(obj) + '>')

        else:
            uris.append('<' + str(obj) + '>')
            owl['<' + str(subj) + '>'] = '<' + str(obj) + '>'

    # remove duplicates
    uris = list(dict.fromkeys(uris))
    # lookup uris
    new_uris = lookup_lincs_kg(uris)

    # check if replacements already exists in file
    duplicates = []
    for subj, pred, obj in g:
        for subject in new_uris:
            if str(subject) == str(subj):
                if str(new_uris[subject]) == str(obj):
                    duplicates.append(subject)
            if str(subject) == str(obj):
                if str(new_uris[subject]) == str(subj):
                    duplicates.append(subject)

    g = Graph()
    g.bind('owl', 'http://www.w3.org/2002/07/owl#')
    g.bind('lincs', 'http://id.lincsproject.ca/')
    for subj in owl:
        count = ''
        lquery = '''PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                    PREFIX lincs: <http://id.lincsproject.ca/>
                    PREFIX owl:<http://www.w3.org/2002/07/owl#>
                    PREFIX viaf: <http://viaf.org/viaf/>
                    PREFIX wd: <http://www.wikidata.org/entity/>
                    SELECT (COUNT(*) AS ?triples)
                    WHERE {
                        ?subject ?pred ?obj .
                    }'''
        lincsquery = Query(endpoint="https://rs.stage.lincsproject.ca/sparql", query=lquery, authority='none')
        results = lincsquery.get_count(subj)
        for result in results:
            try:
                count = result['triples']['value']
            except Exception as e:
                print('Error')
                print(e)
        if int(count) > 1:

            s = subj.strip('<').strip('>')
            o = new_uris[owl[subj].strip('<').strip('>')]
            if s == o:
                continue
            g.add((URIRef(s), URIRef('http://www.w3.org/2002/07/owl#sameAs'), URIRef(o)))
            g.add((URIRef(o), URIRef('http://www.w3.org/2002/07/owl#sameAs'), URIRef(s)))
            new_uris.pop(subj, None)
        else:
            try:
                new_uris[subj] = new_uris[owl[subj]]
            except Exception as e:
                print(e)

    # saving files in the data/minting directory
    dir_path = create_data_dir('lookup_', '/code/app/data/kg_lookup')

    # save owl:sameAs output
    if len(g) > 0:
        with open(dir_path + "/owlsameAs.ttl", "w", encoding=encoding) as f:
            f.write(g.serialize(format='ttl'))

    # remove duplicates
    for item in duplicates:
        new_uris.pop(item)
    # save replacements to json file
    with open(dir_path + "/lookup_mappings.json", "w") as f:
        json.dump(new_uris, f)

    # saving reference to response object
    response = return_lookup_file(dir_path)

    # cleanup
    shutil.rmtree(dir_path)

    return response
