import json
import shutil
import urllib

from fastapi import APIRouter, UploadFile, Form
from typing import Annotated

from app.utils.utils import create_data_dir, cleanup_files
from app.utils.graph_helpers import create_graph
from app.utils.validation_helpers import riot_validate, get_used_prefixes, validate_prefixes, validate_uris, vocab_validation, cidoc_validation, sparql_validation


"""
TODO:
- test which input rdf serializations this works for and list the possible input values.
"""


router = APIRouter()


@router.post("", status_code=200)
@router.post("/", status_code=200)
def validate(
	files: list[UploadFile],
	encoding: Annotated[str, Form(...)] = "utf-8",
	rdf_format: Annotated[str, Form(...)] = "turtle",
	analyze: Annotated[bool, Form(...)] = True):

	# we store the input files until this job is complete
	# we store all validation results for this job until the job is complete
	output_path = create_data_dir("", "/code/app/data/validation/")
	input_files = {}
	for file in files:
		filename = file.filename
		input_temp_path = output_path + f"/input_{filename}"
		input_files[filename] = input_temp_path

		# temporarily save file to validate
		with open(input_temp_path, "wb") as buffer:
			shutil.copyfileobj(file.file, buffer)

	# body of response will be the results from RIOT
	riot_result = {}

	# RIOT validation
	print("RIOT Validation")
	global_error_tracker = 0
	accepted_formats = ["turtle", "ntriples", "nquads", "trig", "rdfxml"]
	if rdf_format not in accepted_formats:
		riot_result = {"request_error": f"rdf_format must have one of the following values: {str(accepted_formats)}"}
		rdf_format = ""

	for filename in input_files:
		riot_result[filename], riot_error_tracker = riot_validate(filename, input_files[filename], encoding, rdf_format)
		global_error_tracker = global_error_tracker + riot_error_tracker

	with open(urllib.parse.urljoin(output_path, "/riot_results.json"), "w", encoding=encoding) as result_out:
		json.dump(riot_result, result_out, ensure_ascii=False, indent=4)

	# stop validation checks if RIOT said any of the files were not syntactically valid
	if global_error_tracker > 0 or analyze is False:
		# cleanup
		cleanup_files(output_path)
		return riot_result

	# we now know each file is syntactically valid
	# we combine all of the input RDF files into one graph so we can run the rest of the analysis on it
	# using oxigraph setting for RDFLib as it makes the queries faster. Loading the graph is a bit slower though.
	graph = create_graph(input_files, oxigraph=True)

	# namespace validation
	print("Namespace Validation")
	used_prefixes = get_used_prefixes(graph, output_path)
	riot_result["namespace_validation"] = validate_prefixes(used_prefixes)

	# URI validation
	print("URI Validation")
	riot_result["uri_validation"] = validate_uris(graph)

	# LINCS vocabulary validation
	print("LINCS Vocab Validation")
	riot_result["lincs_vocabulary_validation"] = vocab_validation(graph)

	# CIDOC validation
	# used these regex patterns on
	# https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/Ontologies/CIDOC_CRM_v7.1.1.rdfs
	# then deduplcated and saved in app/data/validation/cidoc_crm_uris.txt
	# rdf:about="(E[0-9]+_.+?)"
	# rdf:about="(P[0-9]+i{0,1}_.+?)"
	print("CIDOC CRM Validation")
	riot_result["cidoc_uri_validaton"] = cidoc_validation(graph)

	# SPARQL validation
	print("SPARQL Validation")
	riot_result["sparql_validaton"] = sparql_validation(graph)

	# cleanup
	cleanup_files(output_path)

	# replace added prefixes of the form
	# file:///code/app/data/validation/2023-11-06_06-08-13/
	riot_result = json.loads(json.dumps(riot_result).replace(f"file://{output_path}/", ""))

	return riot_result
