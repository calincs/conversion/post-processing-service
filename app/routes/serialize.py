import os
import shutil
import subprocess

from fastapi import APIRouter, UploadFile, Form
from fastapi.responses import FileResponse
from typing import Annotated
from starlette.background import BackgroundTask

from app.utils.utils import create_data_dir, cleanup_files, get_rdf_suffix, replace_file_extension


router = APIRouter()


# format options listed here: https://jena.apache.org/documentation/io/
@router.post("", status_code=200)
@router.post("/", status_code=200)
def serialize(
        file: UploadFile,
        input_serialization: Annotated[str, Form(...)] = "",
        output_serialization: Annotated[str, Form(...)] = "turtle",
        encoding: Annotated[str, Form(...)] = "utf-8"):

    accepted_formats = ["turtle", "ntriples", "nquads", "trig", "rdfxml"]

    if output_serialization not in accepted_formats:
        output_serialization = "turtle"

    data_path = create_data_dir("", "/code/app/data/serialize/")
    input_file = data_path + "/" + file.filename
    output_file_extension = get_rdf_suffix(output_serialization)
    output_filename = replace_file_extension(file.filename, output_file_extension)
    output_path = data_path + "/" + output_filename

    # change working dir to apache file
    jena_path = os.environ.get("JENA_HOME")
    os.chdir(f'{jena_path}/bin')

    # we store the input files until this job is complete
    # we store all validation results for this job until the job is complete
    with open(input_file, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    # command to create executable
    os.system('chmod u+x ./riot')

    # Build Apache Jena Riot Command
    # for example, "./riot --output=rdfxml --syntax=ntriples Persons.nt"
    command_list = ['./riot']
    if input_serialization != "" and input_serialization in accepted_formats:
        command_list.append(f'--syntax={input_serialization}')
    command_list.append(f'--output={output_serialization}')
    command_list.append(input_file)

    # run riot command
    # save output file
    with open(output_path, 'w', encoding=encoding) as f:
        subprocess.run(command_list, stdout=f)

    # Replace namespaces like "file:///code/app/data/combine/2023-11-04_08-25-25/
    # .replace(f"file://{output_path}/", "")
    with open(output_path, "r", encoding=encoding) as f_in:
        f_contents = f_in.read()
    f_contents = f_contents.replace(f"file://{data_path}/", "")
    with open(output_path, "w", encoding=encoding) as f_out:
        f_out.write(f_contents)

    # return the converted file and delete it from the container
    # TODO update output file extension
    if os.path.isfile(output_path):
        return FileResponse(path=output_path, filename=output_filename, background=BackgroundTask(cleanup_files, data_path))

    else:
        return None
