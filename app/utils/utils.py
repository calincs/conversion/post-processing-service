import io
import zipfile
import os
import datetime
import json
import re
import shutil
import requests

from pathlib import Path
from starlette.responses import Response


def get_prefix(link, file_contents):
    '''retrieves prefix for chosen authority'''
    # finds all regex matches of the defined prefix using the uri
    matches = re.findall(r'@prefix (.*?):(\s+)<{}> .'.format(re.escape(link)), file_contents)
    prefixes = []
    if matches:
        prefixes = matches[0]
    for prefix in prefixes:
        if not prefix:
            # if no prefix is found, replace 'http' in the uri with 'https' and try the regex search again
            prefixes = re.findall(r'@prefix (.*?):(\s+)<{}>(\s+).'.format(re.escape(link.replace('http', 'https'))), file_contents)[0]
        return prefix


def get_ids(link, file_contents, prefix, pattern):
    '''extracts URIs matching the chosen authority'''
    ids = []
    if file_contents:
        # if a prefix is found for the entity, use it in the regex search to find ids starting with the prefix
        if prefix:
            ids += list(set(re.findall(r"{}:({}[0-9]+)".format(re.escape(prefix), re.escape(pattern)), file_contents)))
        # catch URIs in full
        ids += list(set(re.findall(r"{}({}[0-9]+)".format(re.escape(link), re.escape(pattern)), file_contents)))
        ids += list(set(re.findall(r"{}({}[0-9]+)".format(re.escape(link.replace('http', 'https')), re.escape(pattern)), file_contents)))

    return ids


def return_file(filename, file_path, g, prop_warning=None, encoding="utf-8"):
    '''returns data and mapping file zipped together'''

    with open(filename, "w", encoding=encoding) as f:
        # serialize the graph and write to file
        f.write(g.serialize(format='turtle'))
        if prop_warning:
            f.write('\n# WARNING: Wikidata properties typically should not be used in LINCS data. It is better to find an equivalent wikidata entity with a Q ID. \n')

    # Zipping files test
    zip_filename = "output.zip"
    s = io.BytesIO()
    zf = zipfile.ZipFile(s, "w")

    # Add file, at correct path
    zf.write(filename, filename.split("/")[-1])
    if os.path.exists(file_path + '/redirects.json'):
        zf.write(file_path + '/redirects.json', 'redirects.json')

    if os.path.exists(file_path + '/missing_labels_or_entities.json'):
        zf.write(file_path + '/missing_labels_or_entities.json', 'missing_labels_or_entities.json')

    # Must close zip for all contents to be written
    zf.close()

    # Grab ZIP file from in-memory, make response with correct MIME-type
    resp = Response(s.getvalue(), media_type="application/x-zip-compressed", headers={
        'Content-Disposition': f'attachment;filename={zip_filename}'
    })

    return resp


def redirect_file(redirects, output_path):
    '''fetches redirects if any and writes them to redirects.json'''
    # redirect mapping file
    # If an authority has a redirect, write the json
    json_wrote = False
    for auth in redirects.redirect_json:
        if len(redirects.redirect_json[auth]) > 0:
            if json_wrote is False:
                with open(output_path + '/redirects.json', 'w') as fp:
                    json.dump(redirects.redirect_json, fp)
                json_wrote = True
        else:
            print("0 redirects found for {}".format(auth))


def skip_owl(skip_predicate):
    '''parses the skip_predicate list and checks for the owl:sameAs predicate'''
    # new list of predicates to be skipped
    predicates = []
    for predicate in skip_predicate:
        # if owl:sameAs predicate is found, it adds the different forms to the list of predicates to be skipped
        if predicate.lower() == 'http://www.w3.org/2002/07/owl#sameas' or predicate.lower() == 'owl:sameas':  
            predicates.extend(('owl:sameas', 'owl:sameAs', 'http://www.w3.org/2002/07/owl#sameAs'))  # case-insensitive
        else:
            predicates.append(predicate)
    return predicates


def create_data_dir(path, endpoint):
    '''
    creates data directory

    endpoint is string of the form 'app/data/labels'
    path is string of the form 'wikidata_'
    '''
    Path(endpoint).mkdir(parents=True, exist_ok=True)
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    new_path = os.path.join(endpoint, path + timestamp)
    Path(new_path).mkdir(parents=True, exist_ok=True)
    return new_path


def status_code_chk(r):
    '''
    Checks if an http response's status code is a 400 level code.
    '''
    if str(r.status_code).startswith('4'):
        return False
    return True


def geonames_response_body_chk(ids, link):
    non_existing_entities = []
    for id in ids:
        r = requests.get(link + id + "/about.rdf")
        if str(r.status_code) == '404' or 'for input string:' in str(r.text):
            non_existing_entities.append("geonames:" + id)


def cleanup_files(path):
    '''deletes the file specified with path'''
    shutil.rmtree(path)


def get_rdf_suffix(rdf_format: str):
    # Jena RIOT and rdflib rdf formats to the corresponding file extension
    if rdf_format in ["turtle", "ttl"]:
        return ".ttl"
    elif rdf_format in ["ntriples", "nt"]:
        return ".nt"
    elif rdf_format in ["nquads"]:
        return ".nq"
    elif rdf_format in ["trig"]:
        return ".trig"
    elif rdf_format in ["rdfxml", "xml", "pretty-xml"]:
        return ".xml"
    elif rdf_format in ["json-ld"]:
        return ".json"
    elif rdf_format in ["n3"]:
        return ".n3"
    else:
        return ".ttl"



def replace_file_extension(filename: str, new_extension: str):
    try:
        split = filename.split(".")
        old_extension = "." + split[-1]
        filename = filename.replace(old_extension, new_extension)
    except:
        filename = filename + new_extension

    return filename