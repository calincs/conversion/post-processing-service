from rdflib import Graph
from fastapi import HTTPException


# load one or more files into rdflib graph in memory
# input_files is a dict {filename: path to file, ...}
def create_graph(input_files: dict, oxigraph=False):
	if oxigraph:
		g = Graph(store="Oxigraph")
	else:
		g = Graph()

	for filename in input_files:
		try:
			g.parse(input_files[filename])
		except Exception as err:
			print("Error parsing file, check if input file is valid RDF. ", err)
			raise HTTPException(status_code=400, detail=f"Cannot parse input file {filename}")

	return g


def serialize_graph(g, output_path, out_format):
	out_format = out_format.replace(".", "")
	g.serialize(destination=output_path, format=out_format)


def get_all_entities(g, p=True):
	# returns list of all entities in a graph that are URIs
	entities = []

	if p:
		search_vars = ["?s", "?p", "?o"]
	else:
		search_vars = ["?s", "?o"]

	for q in search_vars:
		query = f"SELECT DISTINCT ({q} as ?r) WHERE {{?s ?p ?o . FILTER(isIRI({q})) .}}"

		qres = g.query(query)
		for row in qres:
			entities.append(str(row.r))

	entities = list(set(entities))
	return entities


def get_entities_contain_string(g, search_string, p=True):
	# returns list of all entities in a graph that are URIs that contain the input string
	# does not get properties. subject and objects of triples only
	entities = []

	if p:
		search_vars = ["?s", "?p", "?o"]
	else:
		search_vars = ["?s", "?o"]

	for q in search_vars:

		query = f'SELECT DISTINCT ({q} as ?r) WHERE {{?s ?p ?o . FILTER(isIRI({q})) . FILTER(regex(str({q}), "{search_string}" ) ) .}}'
		qres = g.query(query)

		for row in qres:
			entities.append(str(row.r))

	entities = list(set(entities))
	return entities
