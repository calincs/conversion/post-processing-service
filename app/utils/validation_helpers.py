import json
import os
import re
import subprocess
import time

from difflib import SequenceMatcher
from rdflib import Graph

from app.utils.label_queries import Query


# the input files are UploadFile objects (file like objects)
def riot_validate(filename, filepath, encoding: str, rdf_format: str):
	start = time.time()

	error_tracker = 0

	result = {'status': '', 'errors': ""}

	# change working dir to apache file
	jena_path = os.environ.get("JENA_HOME")
	os.chdir(f'{jena_path}/bin')

	# command to create executable
	os.system('chmod u+x ./riot')

	format_parameter = ["", ""]
	if rdf_format != "":
		format_parameter = ["--formatted", rdf_format]

	# run riot command
	p = subprocess.run(
		['./riot', format_parameter[0], format_parameter[1], '--validate', filepath],
		shell=False,
		capture_output=True)

	if p.stderr:
		result['status'] = "This file is NOT valid."
		result['errors'] = p.stderr.splitlines()[0].decode(encoding)
		error_tracker += 1
	else:
		result['status'] = "This file is valid."
	print(time.time() - start)
	return result, error_tracker


def get_used_prefixes(g, output_path):
	# Get namespaces for the input graph
	# Replace namespaces like "file:///code/app/data/validation/2023-11-04_08-25-25/
	namespaces = []
	for ns_prefix, namespace in g.namespaces():
		namespaces.append(str(namespace).replace(f"file://{output_path}/", ""))

	return list(set(namespaces))


def get_valid_prefixes():
	# get approved namespaces we have saved in our list
	with open("/code/app/data/validation/approved_prefixes.txt", 'r') as f:
		valid_prefixes = [prefix.strip('\n') for prefix in f]

	# also get default rdflib namespaces
	g_temp = Graph()

	for ns_prefix, namespace in g_temp.namespaces():
		valid_prefixes.append(str(namespace))
	return valid_prefixes


def validate_prefixes(used_prefixes):
	start = time.time()
	valid_prefixes = get_valid_prefixes()

	result = {'errors': []}
	errors = {}

	for used_prefix in used_prefixes:

		# handle invalid prefixes
		if used_prefix not in valid_prefixes:

			# check if prefix is an error/typo
			# store close matches for this prefix
			errors[used_prefix] = []
			for valid_prefix in valid_prefixes:
				ratio = SequenceMatcher(None, used_prefix, valid_prefix).ratio()
				if ratio > 0.80:
					errors[used_prefix].append(valid_prefix)

	# write result to output
	result['errors'] = []
	if errors:
		for used_prefix, matches in errors.items():
			if matches != []:
				error_dict = {'unrecognized_namespace': used_prefix, 'suggested_namespaces': list(set(matches))}
			else:
				error_dict = {'unrecognized_namespace': used_prefix}
			result['errors'].append(error_dict)
	print(time.time() - start)
	return result


def validate_uris(g):
	start = time.time()
	errors = {}

	# get approved namespaces we have saved in our list
	with open("/code/app/data/validation/uri_patterns.json", 'r') as f:
		pattern_list = json.loads(f.read())

	uris = []
	for subj, pred, obj in g:
		uris.append(str(subj))
		uris.append(str(obj))

	uris = list(set(uris))

	for pattern_dict in pattern_list:
		temp_errors = []
		for uri in uris:
			# handle accepted patterns
			# if none of the accepted patterns are found in the URI then add it to the error list
			# once one is found, stop and add it to the list

			if pattern_dict["contains"] in uri.lower():
				match_found = False
				for accepted_pattern in pattern_dict["accepted_patterns"]:
					match = re.fullmatch(accepted_pattern, uri)
					if match is not None:
						match_found = True
						break
				if not match_found:
					temp_errors.append(uri)

				# if any of the error patterns are found, stop and add it to the list
				for error_pattern in pattern_dict["error_patterns"]:
					match = re.search(error_pattern, uri)
					if match is not None:
						temp_errors.append(uri)
						break

		if temp_errors != []:
			errors[pattern_dict["name"]] = list(set(temp_errors))
	print(time.time() - start)
	return {'errors': errors}


def vocab_validation(g):
	start = time.time()
	# get LINCS vocab namespaces
	lincs_prefixes = []
	with open("/code/app/data/validation/lincs_vocabs.txt", 'r') as f:
		for line in f.readlines():
			lincs_prefixes.append(line.strip("\n"))

	# parse the input graph to get URIs that start with any of the LINCS vocab namespaces
	used_lincs_uris = []
	for subj, pred, obj in g:
		for i in [subj, pred, obj]:
			if re.fullmatch(r"http:\/\/id\.lincsproject\.ca\/.+\/.+", i) is not None:
				used_lincs_uris.append(str(i))

	used_lincs_uris = list(set(used_lincs_uris))

	errors = []
	for uri in used_lincs_uris:
		squery = f"SELECT DISTINCT * WHERE {{ <{uri}> ?pred ?obj .}} LIMIT 1"
		query = Query(endpoint="https://fuseki.lincsproject.ca/lincs/", query=squery, authority=None)
		results = query.get_results()
		if results is None:
			errors.append(uri)
		elif len(results) == 0:
			errors.append(uri)
	print(time.time() - start)

	return {"error": errors}


def cidoc_validation(g):
	start = time.time()
	# check if there are CIDOC CRM entities or properties used in the input graph that are not in the CIDOC ontology
	errors = []

	# get the CIDOC URIs used in the input graph
	used_cidoc_uris = []
	for subj, pred, obj in g:
		for i in [subj, pred, obj]:
			if re.search(r"http://www.cidoc-crm.org/cidoc-crm/[EP]", i) is not None:
				used_cidoc_uris.append(str(i))

	used_cidoc_uris = list(set(used_cidoc_uris))

	# get the valid list of CIDOC URIs
	valid_cidoc_uris = []
	with open("/code/app/data/validation/cidoc_crm_uris.txt", 'r') as f:
		for line in f.readlines():
			valid_cidoc_uris.append("http://www.cidoc-crm.org/cidoc-crm/" + line.strip("\n"))

	for uri in used_cidoc_uris:
		if uri not in valid_cidoc_uris:
			errors.append(uri)

	print(time.time() - start)
	return {'errors': errors}


def sparql_validation(g):
	start = time.time()
	sparql_results = {}

	# get list of entities that are rdf:type crm:E73_Information_Object
	E73s = []
	# get list of entities that are only the objects of owl:sameAs
	owl_sameAs_all = []  # any object of owl:sameAs
	subjs_all = []  # any subject of a triple
	owl_sameAs = []  # only the owl:sameAs objects that are not also triple subjects
	# note that if an entity is the subject of a owl:sameAs then it will still count as a subject

	for subj, pred, obj in g:
		subjs_all.append(str(subj))
		if str(pred) == "http://www.w3.org/1999/02/22-rdf-syntax-ns#type":
			if str(obj) == "http://www.cidoc-crm.org/cidoc-crm/E73_Information_Object":
				E73s.append(str(subj))
		if str(pred) in ["http://www.w3.org/2002/07/owl#sameAs", "https://www.w3.org/2002/07/owl#sameAs"]:
			owl_sameAs_all.append(str(obj))

	subjs_all = list(set(subjs_all))
	for owl in owl_sameAs_all:
		if owl not in subjs_all:
			owl_sameAs.append(owl)
	print(time.time() - start)

	"""
	entities that are missing rdfs:label
	TODO could return their rdfs:type values if they have any. But query is slower
	they don't need a label if it's only ever the object of an owl:sameAs or is rdf:type crm:E73_Information_Object

	"""

	# all subjects and objects without labels
	print("Query: subjects and objects without labels")
	start = time.time()
	missing_labels = {}
	for var in ["?subject", "?object"]:

		query = f"""PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

				SELECT DISTINCT ({var} as ?uri) ?type
				WHERE {{
				    ?subject ?predicate ?object .
				    OPTIONAL {{{var} rdf:type ?type}}
				    FILTER NOT EXISTS {{{var} rdfs:label ?label}}
				    FILTER (!isLiteral({var})) .
				}}"""

		qres = g.query(query)

		# return the query results, grouping the rdf:type by entity
		for row in qres:
			# remove entities if they are only the object of owl:sameAs
			# remove entities if they are of rdf:type crm:E73_Information_Object
			if str(row.uri) not in owl_sameAs and str(row.uri) not in E73s and "cidoc-crm" not in str(row.uri):
				if str(row.uri) in missing_labels:
					if str(row.type) not in missing_labels[str(row.uri)]["rdf_types"]:
						missing_labels[str(row.uri)]["rdf_types"].append(str(row.type))
				else:
					if str(row.type) == "None":
						missing_labels[str(row.uri)] = {"rdf_types": []}
					else:
						missing_labels[str(row.uri)] = {"rdf_types": [str(row.type)]}

	sparql_results["missing_labels"] = missing_labels
	print(time.time() - start)

	"""
	entities that are missing rdf:type with their URIs and their rdfs:label values if they have any
	they don't need rdf:type if it's only ever the object of an owl:sameAs triple

	"""

	# all subjects and objects without types
	print("Query: subjects and objects without types")
	start = time.time()
	missing_types = {}
	for var in ["?subject", "?object"]:

		query = f"""PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
				PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

				SELECT DISTINCT ({var} as ?uri) ?label
				WHERE {{
				    ?subject ?predicate ?object .
				    OPTIONAL {{{var} rdfs:label ?label}}
				    FILTER NOT EXISTS {{{var} rdf:type ?type}}
				    FILTER (!isLiteral({var})) .
				}}"""

		qres = g.query(query)

		# return the query results, grouping the rdf:label by entity
		for row in qres:
			# remove entities if they are only the object of owl:sameAs
			if str(row.uri) not in owl_sameAs and "cidoc-crm" not in str(row.uri):
				if str(row.uri) in missing_types:
					if str(row.label) not in missing_types[str(row.uri)]["rdfs_labels"]:
						missing_types[str(row.uri)]["rdfs_labels"].append(str(row.label))
				else:
					if str(row.label) == "None":
						missing_types[str(row.uri)] = {"rdfs_labels": []}
					else:
						missing_types[str(row.uri)] = {"rdfs_labels": [str(row.label)]}

	sparql_results["missing_types"] = missing_types
	print(time.time() - start)


	"""
	rdfs:label values that are missing language tags
	grouped by entity

	"""
	print("Query: rdfs:label values that are missing language tags")
	start = time.time()

	labels_no_lang = {}
	query = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
			SELECT DISTINCT *
			WHERE {
			  ?uri rdfs:label ?label .
			 FILTER(LANG(?label) = "")
			}
	"""
	qres = g.query(query)

	for row in qres:
		if str(row.uri) in labels_no_lang:
			labels_no_lang[str(row.uri)]["rdfs_labels_no_language"].append(str(row.label))
		else:
			if str(row.label) == "None":
				labels_no_lang[str(row.uri)] = {"rdfs_labels_no_language": []}
			else:
				labels_no_lang[str(row.uri)] = {"rdfs_labels_no_language": [str(row.label)]}

	sparql_results["labels_missing_language_tag"] = labels_no_lang
	print(time.time() - start)


	"""
	two separate entities that share an rdfs:label (match if lower case is a match)
	"""
	print("Query: two separate entities that share an rdfs:label")
	start = time.time()
	shared_labels = {}
	labels = {}
	for subj, pred, obj in g:
		if "http://www.w3.org/2000/01/rdf-schema#label" in str(pred):
			if str(obj).lower() in labels:
				labels[str(obj).lower()].append(str(subj))
			else:
				labels[str(obj).lower()] = [str(subj)]

	for label in labels:
		dedupe = list(set(labels[label]))
		if len(dedupe) > 1:
			shared_labels[label] = dedupe

	sparql_results["shared_labels_lowercased"] = shared_labels
	print(time.time() - start)


	"""
	one entity with duplicate labels except the letter case is different
	"""
	print("Query: one entity with duplicate labels except the letter case is different")
	start = time.time()
	duplicate_labels = {}
	query = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
			SELECT DISTINCT *
			WHERE {
			  ?uri rdfs:label ?label1 .
			  ?uri rdfs:label ?label2 .
			  FILTER (str(?label1) != str(?label2))
			  FILTER (lcase(str(?label1)) = lcase(str(?label2)))
			}"""
	qres = g.query(query)

	for row in qres:
		if str(row.uri) in duplicate_labels:
			if str(row.label1) not in duplicate_labels[str(row.uri)]["labels"]:
				duplicate_labels[str(row.uri)]["labels"].append(str(row.label1))
			if str(row.label2) not in duplicate_labels[str(row.uri)]["labels"]:
				duplicate_labels[str(row.uri)]["labels"].append(str(row.label2))
			duplicate_labels[str(row.uri)]["labels"] = list(set(duplicate_labels[str(row.uri)]["labels"]))
		else:
			duplicate_labels[str(row.uri)] = {"labels": [str(row.label1), str(row.label2)]}

	sparql_results["duplicate_labels_lowercased"] = duplicate_labels
	print(time.time() - start)


	"""
	entities that have at least two rdf:type values
	with their rdfs:labels if they have them
	group them by the combination of types they have
	"""
	print("Query: entities that have at least two rdf:type values")
	start = time.time()
	multiple_types = {}
	query = """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
			SELECT DISTINCT ?uri ?type1 ?type2 ?label
			    WHERE {
			        ?uri rdf:type ?type1 .
			  		?uri rdf:type ?type2 .
			  		OPTIONAL {?uri rdfs:label ?label .}
			  		FILTER (str(?type1) > str(?type2))
			}"""

	qres = g.query(query)

	for row in qres:
		label = str(row.label)
		if str(row.label) == "None":
			label = ""

		sorted_list = "||".join(sorted([str(row.type1), str(row.type2)]))
		if sorted_list in multiple_types:
			multiple_types[sorted_list][str(row.uri)] = label
		else:
			multiple_types[sorted_list] = {str(row.uri): label}

	sparql_results["multiple_types"] = multiple_types

	print(time.time() - start)
	return sparql_results
