import requests
import xml.etree.ElementTree as ET


def get_authority_redirect(self, ids, authority_url, authority_prefix, url_split_token, url_split_index, url_split_type):
    '''
    Generic redirect search that works for loc, getty, geonames
    wikidata and viaf have unique needs so are defined separately
    '''
    for entity in ids:
        response = requests.get(authority_url + str(entity))
        url = response.url
        # The additional searches other than the 404 are for geonames
        if response.status_code == 404 or 'For input string:' in str(response.text) or 'this id does not exist' in str(response.text):
            continue

        if url_split_type == "single":
            new_entity = url.rsplit(url_split_token, 1)[url_split_index].strip('.html')
        elif url_split_type == "all":
            new_entity = url.rsplit(url_split_token)[url_split_index].strip('.html')

        if new_entity != entity:
            self.redirect_json[authority_prefix + entity] = authority_prefix + new_entity
            self.new_file = self.new_file.replace(entity, new_entity)
        else:
            continue


class Redirects:

    def __init__(self):
        # JSON for tracking all redirects done to file
        self.redirect_json = {}
        self.new_file = None

        # Wikidata API variables
        self.max_wiki_ids = 50
        self.wiki_endpoint = "https://www.wikidata.org/w/api.php"
        self.wiki_params = {'action': 'wbgetentities',
                            'ids': None,
                            'format': 'json',
                            'props': 'sitelinks'}

        # VIAF API variables
        self.ns = {'v': 'http://viaf.org/viaf/terms#',
                   'rd': 'http://viaf.org/viaf/abandonedViafRecord'}

    def get_wikidata_redirect(self, ids, root_id=None):
        """
        Takes in a list of IDs and calls Wikidata api with up to 50 ids checking if any are redirected to another ID.
        Also updates the redirect_json file with mappings of root_ids -> final redirect.  If a redirect is found it will
        recursively search if that redirect has any further redirects (I doubt this is possible but better safe than
        sorry? --Justin (Summer Intern 2021)
        :param root_id: Root ID if recursing through multiple redirects / function calls
        :param ids: List of IDs
        :return:
        """
        bulk_ids = ''
        id_count = 0

        for id in ids:
            bulk_ids += id + '|'
            id_count += 1
            if id_count == self.max_wiki_ids or id == ids[-1]:
                self.wiki_params['ids'] = bulk_ids[:-1]
                r = requests.get(self.wiki_endpoint, params=self.wiki_params)

                while 'error' in list(r.json().keys()):
                    remove_id = r.json()['error']['id']
                    bulk_ids = bulk_ids.replace("|" + remove_id, "")
                    self.wiki_params['ids'] = bulk_ids[:-1]
                    r = requests.get(self.wiki_endpoint, params=self.wiki_params)

                entities = r.json()['entities']
                for entity in entities:
                    redirect = entities[entity].get('redirects')
                    if redirect is not None:
                        # Store redirect in json using root_id (if first call root_id is just the id in the loop)
                        if root_id is None:
                            entity_name = "wikidata:" + entity
                            self.redirect_json[entity_name] = "wikidata:" + redirect['to']
                            # Update new file with new redirect
                            self.new_file = self.new_file.replace(entity, redirect['to'])
                            # Redirect recursion loop
                            self.get_wikidata_redirect([redirect['to']], root_id=entity)
                        else:
                            entity_name = "wikidata:" + root_id
                            self.redirect_json[entity_name] = "viaf:" + redirect['to']
                            # Update new file with new redirect
                            self.new_file = self.new_file.replace(entity, redirect['to'])
                            # Redirect recursion loop
                            self.get_wikidata_redirect([redirect['to']], root_id=root_id)

                # Reset variables
                bulk_ids = ''
                id_count = 0

    def get_viaf_redirect(self, ids, root_id=None):
        """
        Iterates through individual ids in a list checking if a VIAF redirect exists will update file and redirects_json
        file
        :param ids: List of ids to check
        :param root_id: Root id if recursing through possible redirects.
        :return:
        """

        for entity in ids:
            response = requests.get("http://www.viaf.org/viaf/{}/viaf.xml".format(entity))
            root = ET.fromstring(response.text)
            if root.tag == '{http://viaf.org/viaf/abandonedViafRecord}abandoned_viaf_record':
                redirect = root.find("rd:redirect/rd:directto", self.ns)
                if redirect is not None:
                    if root_id is None:
                        entity_name = "viaf:" + entity
                        self.redirect_json[entity_name] = "viaf:" + redirect.text
                        self.new_file = self.new_file.replace(entity, redirect.text)
                        self.get_viaf_redirect([redirect.text], root_id=entity)
                    else:
                        entity_name = "viaf:" + entity
                        self.redirect_json[entity_name] = "viaf:" + redirect.text
                        self.new_file = self.new_file.replace(entity, redirect.text)
                        self.get_viaf_redirect([redirect.text], root_id=root_id)

    def get_dbpedia_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://dbpedia.org/resource/", "dbpedia:", "/", -1, "single")

    def get_loc_redirects(self, prefix, link, ids, root_id=None):
        get_authority_redirect(self, ids, link, prefix, "/", -1, "single")

    def get_locnames_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://id.loc.gov/authorities/names/", "locnames:", "/", -1, "single")

    def get_locworks_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://id.loc.gov/resources/works/", "locworks:", "/", -1, "single")

    def get_locsubjects_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://id.loc.gov/authorities/subjects/", "locsubjects:", "/", -1, "single")

    def get_locprovs_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://id.loc.gov/entities/providers/", "locprovs:", "/", -1, "single")

    def get_locinsts_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://id.loc.gov/resources/instances/", "locinsts:", "/", -1, "single")

    def get_loclangs_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://id.loc.gov/vocabulary/languages/", "loclangs:", "/", -1, "single")

    def get_locgenreForms_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://id.loc.gov/authorities/genreForms/", "locgenreForms:", "/", -1, "single")

    def get_geonames_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "https://sws.geonames.org/", "geonames:", "/", -2, "all")

    def get_getty_aat_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://vocab.getty.edu/aat/", "http://vocab.getty.edu/aat/", "/", -1, "single")

    def get_getty_tgn_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://vocab.getty.edu/page/tgn/", "http://vocab.getty.edu/page/tgn/", "=", -1, "single")

    def get_getty_ulan_redirect(self, ids, root_id=None):
        get_authority_redirect(self, ids, "http://vocab.getty.edu/page/ulan/", "http://vocab.getty.edu/page/ulan/", "=", -1, "single")

    # def get_getty_cona_redirect(self, ids, root_id=None):
    #     get_authority_redirect(self, ids, "http://vocab.getty.edu/tgn/", "http://vocab.getty.edu/tgn/", "=", -1, "single")
