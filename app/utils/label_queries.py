import logging
import os
import sys
import SPARQLWrapper
import requests
import xml.etree.ElementTree as ET
import re
import json

from rdflib import Graph, Literal, RDFS, URIRef, RDF

logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
                    format="%(asctime)-15s %(levelname)-8s %(message)s")


class Query:
    """
    Query class used for handling wikidata and dbpedia queries.
    """

    def __init__(self, endpoint, query, authority):
        self.authority = authority
        self.endpoint_url = endpoint
        self.user_agent = "LINCS-https://lincsproject.ca//%s.%s" % (sys.version_info[0], sys.version_info[1])
        self.query = query

    def get_results(self,
                    query='',
                    timeout=5000,
                    max_attempts=5,
                    bulk_values=None,
                    split_amount=2):
        """
        Performs Sparql query using self.endpoint_url
        :param query: query string
        :param timeout: timeout for query, defaults to ten seconds
        :param bulk_values: a set of values, if true bulk query will be done and will split up values if it times out
        :param max_attempts: max attempts to try again
        :param split_amount: equal portions that bulk values will be split into
        :return: A Json of mappings of ID -> Label
        """

        if query == '':
            query = self.query

        bulk = ''
        total_values = 1

        try:
            sparql = SPARQLWrapper.SPARQLWrapper(self.endpoint_url, agent=self.user_agent)
            if bulk_values is not None:
                total_values = len(bulk_values)
                all_results = {}
                for id in bulk_values:
                    if self.authority == 'wikidata':
                        bulk += "wd:" + id + " "
                    if self.authority == 'dbpedia':
                        bulk += "dbr:" + id + " "
                    if self.authority == 'lincs':
                        bulk += "lincs:" + id + " "
                    if self.authority == 'none':
                        bulk += id
                sparql.setQuery(query.replace("?ALL_VALUES", bulk))
            else:
                sparql.setQuery(query)
            sparql.setReturnFormat(SPARQLWrapper.JSON)
            sparql.setTimeout(timeout)
            results = sparql.query().convert()
            if results is not None:
                # If initial query is a success return results
                return results['results']['bindings']
            else:
                return None

        except Exception as e:
            # If query fails, keep splitting up data and try for a max_attempts
            attempts = 1

            # exit if function is kg lookup
            if self.authority == 'none':
                return None
            while True:
                if attempts > max_attempts:
                    logging.info("Max query attempts reached")
                    logging.info(query)
                    return None

                # If bulk query failed keep splitting into equal segments
                if bulk_values is not None:
                    bulk_split = bulk.split(" ")[:-1]
                    bulk_size = int(total_values / split_amount)
                    for i in range(0, len(bulk_values), bulk_size):
                        bulk = ''
                        start = i
                        end = i + bulk_size
                        if end > len(bulk_split):
                            end = len(bulk_split)
                        for idx in range(start, end):
                            bulk += bulk_split[idx] + " "
                        try:
                            if start == 0:
                                sparql.setQuery(query.replace("?ALL_VALUES", bulk))
                                all_results = sparql.query().convert()
                                if all_results is not None:
                                    all_results = all_results['results']['bindings']
                                else:
                                    continue
                            else:
                                sparql.setQuery(query.replace("?ALL_VALUES", bulk))
                                append_results = sparql.query().convert()
                                if append_results is not None:
                                    all_results = all_results + append_results['results']['bindings']
                        except Exception as e:
                            # If query fails double the split amount and try again
                            attempts += 1
                            split_amount *= 2
                            break
                    return all_results

                # If not a bulk query, just try again
                else:
                    try:
                        results = sparql.query().convert()
                        return results
                    except:
                        attempts += 1

            logging.info("data had to be split {} times".format(attempts))
            return all_results

    def get_count(self, uri, query='', timeout=5000):
        query = self.query

        try:
            sparql = SPARQLWrapper.SPARQLWrapper(self.endpoint_url, agent=self.user_agent)
            if uri is not None:
                sparql.setQuery(query.replace("?subject", uri))
            else:
                return 'missing uri'
            sparql.setReturnFormat(SPARQLWrapper.JSON)
            sparql.setTimeout(timeout)
            results = sparql.query().convert()
            if results is not None:
                # If initial query is a success return results
                return results['results']['bindings']
            else:
                return 'None'

        except Exception as e:
            # If query fails, keep splitting up data and try for a max_attempts
            print(e)


class ExternalLabels:

    def __init__(self, input_dir, output_dir):

        self.input_dir = input_dir
        self.output_dir = output_dir

        self.max_id_query = 50
        self.files = [i for i in os.listdir(input_dir) if i.endswith('.ttl')]

        # Build queries found in queries folder
        with open(os.path.join(os.getcwd(), "Queries", "wiki_query.rq")) as fp:
            wiki_query = fp.read()

        with open(os.path.join(os.getcwd(), "Queries", "dbr_query.rq")) as fp:
            dbr_query = fp.read()

        # Setup query engines for wiki and dbpedia
        self.wiki_query = Query(endpoint="https://query.wikidata.org/sparql", query=wiki_query, authority='wikidata')
        self.dbr_query = Query(endpoint="http://dbpedia.org/sparql", query=dbr_query, authority='dbpedia')

    def get_sparql_labels(self, authority):
        """
        Creates a rdf graph, gets list of ids and queries for labels in bulk, then writes to graph.  At the end if the
        graph has atleast one triple it will be written by default as input_dir/../external_labels/<authority>_labels.ttl
        :param authority: authority to be used (either wikidata or dbpedia)
        :return:
        """

        g = Graph()
        if authority == 'wikidata':
            g.bind('wd', 'http://www.wikidata.org/entity/')
        if authority == 'dbpedia':
            g.bind('dbr', 'http://dbpedia.org/resource/')

        ids = self.get_ids(authority=authority)

        for idx in range(0, len(ids), self.max_id_query):
            end = idx + self.max_id_query
            if authority == 'wikidata':
                results = self.wiki_query.get_results(bulk_values=ids[idx:end])
            if authority == 'dbpedia':
                results = self.dbr_query.get_results(bulk_values=ids[idx:end])
            for result in results:
                try:
                    g.add((URIRef(result['subject']['value']),
                       RDFS.label,
                       Literal(result['label']['value'], lang=result['label']['xml:lang'])))
                except Exception as e:
                    print('Error adding entity to graph')
                    print(e)

        if len(g) > 0:
            print("{} labels found for {}".format(len(g), authority))
            with open(os.path.join(self.output_dir, '{}_labels.ttl'.format(authority)), 'w') as fp:
                fp.write(g.serialize(format='ttl').decode('utf-8'))
        else:
            print("0 labels found for {}".format(authority))

    def get_viaf_labels(self):
        """
        Gets a list of ids and iterates through each one by one using an http request to get entity info in xml.
        Creates a graph and adds triples to it.
        The name of choice is decided using the name with a date in it with the most sources,
        if no name with date is found the name without a date and the most sources is chosen instead.
        If the label contains a non utf-8 ascii symbol (non english letter) it will not receive a lang=en tag.
        :return:
        """

        g = Graph()
        g.bind('viaf', 'http://viaf.org/viaf/')
        ns = {'v': 'http://viaf.org/viaf/terms#', 'rd': 'http://viaf.org/viaf/abandonedViafRecord'}

        ids = self.get_ids(authority='viaf')

        for id in ids:
            response = requests.get("http://www.viaf.org/viaf/{}/viaf.xml".format(id))
            if response.status_code == 404:
                continue
            root = ET.fromstring(response.text)
            all_data_nodes = root.findall('v:mainHeadings/v:data', ns)

            most_sources = 0
            most_sources_w_date = 0
            name_wo_date = ''
            name_w_date = ''
            for node in all_data_nodes:
                name = node.find('v:text', ns)
                if name is not None:
                    name = name.text
                has_year = any([char.isdigit() for char in name])
                if has_year:
                    num_sources_w_date = len(node.findall('v:sources/v:s', ns))
                    num_sources = 0
                else:
                    num_sources_w_date = 0
                    num_sources = len(node.findall('v:sources/v:s', ns))

                if num_sources > most_sources:
                    most_sources = num_sources
                    name_wo_date = name

                if num_sources_w_date > most_sources_w_date:
                    most_sources_w_date = num_sources_w_date
                    name_w_date = name

            if name_w_date == '':
                if name_wo_date == '':
                    print("no name found for {}".format(id))
                if isEnglish(name_wo_date):
                    o = Literal(name_wo_date, lang='en')
                else:
                    o = Literal(name_wo_date)
            else:
                if isEnglish(name_w_date):
                    o = Literal(name_w_date, lang='en')
                else:
                    o = Literal(name_w_date)
            s = URIRef('http://viaf.org/viaf/{}'.format(id))
            g.add((s, RDFS.label, o))

        if len(g) > 0:
            print("{} Labels found for VIAF".format(len(g)))
            with open(os.path.join(self.output_dir, 'viaf_labels.ttl'), 'w') as fp:
                fp.write(g.serialize(format='ttl').decode('utf-8'))
        else:
            print("0 labels found for VIAF")

    def get_ids(self, authority):
        """
        Iterates through all files in self.files and get ids for given authority.
        :param authority: authority to be used
        :return:
        """
        ids = []
        for file in self.files:
            with open(os.path.join(self.input_dir, file)) as fp:
                file_content = fp.read()
            if authority == 'wikidata':
                ids += list(set(re.findall(r"[wd|wikidata]:(Q[0-9]+)", file_content)))
            if authority == 'dbpedia':
                ids += list(set(re.findall(r"dbr:([A-Za-z_]+)", file_content)))
            if authority == 'viaf':
                ids += list(set(re.findall(r"viaf:([0-9]+)", file_content)))
        return list(set(ids))


def request_loc_labels(ids, exempt_ids, link, g, end, num, rdf_pattern):
    '''retrieves loc labels'''
    missing_entities = []
    for id in ids:
        if id in exempt_ids:
            continue
        # requests rdf version of the respective id's page 
        response = requests.get(link + id + '.nt')
        if not status_code_chk(response):
            missing_entities.append("{}{}".format(link, id))
            continue
        if 'instances' in link:
            link_pattern = rdf_pattern
        else:
            link_pattern = link + id + rdf_pattern
        start = response.text.find(link_pattern) + len(link_pattern)  # index of first occurance
        end2 = response.text.find(end, start) + num  # index of end of line
        tlabel = response.text[start:end2].strip()
        tlabel = tlabel.strip('"')  # remove double quotes
        o = Literal(tlabel, lang='en')
        s = URIRef(link + id)
        g.add((s, RDFS.label, o))

    return missing_entities


def request_getty_labels(ids, exempt_ids, link, g):
    '''retrieves Getty labels'''
    missing_labels = []
    for id in ids:
        if id in exempt_ids:
            continue
        # requests rdf version of the respective id's page 
        request_link = link + id + '.rdf'
        req = requests.get(request_link)
        if not status_code_chk(req):
            missing_labels.append("{}{}".format(link, id))
            continue
        # extract label from rdf response content
        start_s = '<skos:prefLabel xml:lang="en">'
        end_s = '</skos:prefLabel>'
        start = req.text.find(start_s) + len(start_s)
        end = req.text.find(end_s, start)
        label = req.text[start:end].strip()

        # handling different skos pref label tags
        if len(label) > 200 or len(label) == 0:
            start_s = '<prefLabel xmlns="http://www.w3.org/2004/02/skos/core#" xml:lang="en">'
            end_s = '</prefLabel>'
            start = req.text.find(start_s) + len(start_s)
            end = req.text.find(end_s, start)
            label = req.text[start:end].strip()

        # handling different skos pref label tags
        if len(label) > 200 or len(label) == 0:
            start_s = '<rdfs:label xml:lang="en">'
            end_s = '</rdfs:label>'
            start = req.text.find(start_s) + len(start_s)
            end = req.text.find(end_s, start)
            label = req.text[start:end].strip()

        # handling different skos pref label tags
        if len(label) > 200 or len(label) == 0:
            start_s = '<rdfs:label>'
            end_s = '</rdfs:label>'
            start = req.text.find(start_s) + len(start_s)
            end = req.text.find(end_s, start)
            label = req.text[start:end].strip()

        if len(label) < 200 and len(label) != 0:
            o = Literal(label, lang='en')
            s = URIRef(link + id)
            g.add((s, RDFS.label, o))

    return missing_labels


def request_wiki_labels(ids, g, dir_path, lang_code):
    '''retrieves wikidata labels'''
    print('lang_code', lang_code)
    non_existing_json = {"missing_labels_or_entities": []}
    iter = 0
    for idx in range(0, len(ids), 50):
        end = idx + 50
        squery = '''SELECT *
        WHERE {
            VALUES ?subject { ?ALL_VALUES }
            SERVICE wikibase:label {
                bd:serviceParam wikibase:language "[AUTO_LANGUAGE],lang_code" .
                ?subject rdfs:label ?label .
            }
        }'''
        # updates query for specific language code
        squery = squery.replace('lang_code', lang_code)
        wikiquery = Query(endpoint="https://query.wikidata.org/sparql", query=squery, authority='wikidata')
        results = wikiquery.get_results(bulk_values=ids[idx:end])

        # list keeps track of entities missing labels
        missing_labels = []
        for result in results:
            try:
                g.add((
                    URIRef(result['subject']['value']),
                    RDFS.label,
                    Literal(result['label']['value'], lang=result['label']['xml:lang'])))
            except Exception as e:
                # when langcode not = "en", considers redirects as missing ids
                # missing ids already found with langcode = "en", dont need to do this again
                if lang_code == "en":
                    if result['subject']['value'] not in missing_labels:
                        missing_labels.append(result['subject']['value'])
                print('Error adding entity to graph')
                print(e)

        non_existing_json['missing_labels_or_entities'] = non_existing_json['missing_labels_or_entities'] + missing_labels
        iter = iter + 1

    # writes missing labels to missing_labels_or_entities.json
    if non_existing_json['missing_labels_or_entities']:
        with open(dir_path+'/missing_labels_or_entities.json', 'w') as f:
            json.dump(non_existing_json, f)


def request_wiki_coordinates(ids, g, dir_path):
    '''retrieves Wikidata coordinates'''
    iter = 0
    for idx in range(0, len(ids), 50):
        end = idx + 50
        squery = '''SELECT *
        WHERE {
            VALUES ?subject { ?ALL_VALUES }
            ?subject wdt:P625 ?coordinates.}'''

        # updates query for specific language code
        wikiquery = Query(endpoint="https://query.wikidata.org/sparql", query=squery, authority='wikidata')
        results = wikiquery.get_results(bulk_values=ids[idx:end])

        for result in results:
            # add triples of the form
            # wikidata:Q736742 a crm:E53_Place ;
            # crm:P168_place_is_defined_by    "POINT(-0.2171 51.4235)" .
            try:
                g.add((
                    URIRef(result['subject']['value']),
                    RDF.type,
                    URIRef("http://www.cidoc-crm.org/cidoc-crm/E53_Place")))
                g.add((
                    URIRef(result['subject']['value']),
                    URIRef("http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by"),
                    Literal(result['coordinates']['value'].replace("Point", "POINT"))))
            except Exception as e:
                print('Error adding entity to graph')
                print(e)

        iter = iter + 1

        print(g)


def request_dbpedia_labels(ids, g, dir_path, lang_code):
    '''retrieves dppedia labels'''
    non_existing_json = {"missing_labels_or_entities": []}
    iter = 0
    for idx in range(0, len(ids), 50):
        end = idx + 50
        # getting subset of ids that are being sent in request
        sub_ids = ids[(iter * 50):end]
        squery = '''SELECT *
                WHERE {
                    VALUES ?subject { ?ALL_VALUES }
                    ?subject rdfs:label ?label .
                    FILTER (langMatches(lang(?label),"lang_code"))
                }'''
        squery = squery.replace("lang_code", lang_code)
        dbr_query = Query(endpoint="http://dbpedia.org/sparql", query=squery, authority='dbpedia')
        results = dbr_query.get_results(bulk_values=ids[idx:end])
        found_labels = []
        for result in results:
            found_labels.append(result['label']['value'])
            try:
                g.add((
                    URIRef(result['subject']['value']),
                    RDFS.label,
                    Literal(result['label']['value'], lang=result['label']['xml:lang'])))
            except Exception as e:
                print('Error adding entity to graph')
                print(e)
        sub_ids = list(set(sub_ids))
        found_labels = list(set(found_labels))
        missing_labels = list(set(sub_ids) - set(found_labels))
        non_existing_json['missing_labels_or_entities'] = non_existing_json['missing_labels_or_entities'] + missing_labels
        iter = iter + 1
    # dont rewrite to the file twice, only for lang code en
    if non_existing_json['missing_labels_or_entities'] and lang_code == "en":
        with open(dir_path+'/missing_labels_or_entities.json', 'w') as f:
            json.dump(non_existing_json, f, indent=4)


def isEnglish(s):
    # TODO not the best method but works for now
    # Needed to decide when to apply english language code to labels from authorities
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


def status_code_chk(r):
    if str(r.status_code).startswith('4'):
        return False
    return True
